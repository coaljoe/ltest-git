package main

import (
	"fmt"
	"reflect"
)

type Entity interface{}

type SpeedI interface {
	getSpeed() Speed
}

type Speed struct {
	speed float64
}

func (s Speed) getSpeed() Speed { return s }

/*
func getSpeed(en Entity) (Speed, bool) {
	if si, ok := en.(SpeedI); ok {
		return si.getSpeed(), true
	}
	return Speed{}, false
}
*/

func getSpeed(en Entity) Speed {
	if si, ok := en.(SpeedI); ok {
		return si.getSpeed()
	} else {
		panic("component get failed")
	}
}

func hasSpeed(en Entity) bool {
	if _, ok := en.(SpeedI); ok {
		return true
	}
	return false
}

type PositionI interface {
	getPosition() Position
}

type Position struct {
	x, y float64
}

func (p Position) getPosition() Position { return p }

type UnitEntity struct {
	Position
	Speed
}

type BuildingEntity struct {
	Position
}

func main() {
	u1 := UnitEntity{Position{x: 10, y: 10}, Speed{speed: 100}}
	b1 := BuildingEntity{Position{x: 11, y: 11}}

	//arr := []PositionI{u1, b1}
	//for _, x := range arr {
	//	fmt.Println("Position:", x.get())
	//}

	arr2 := []Entity{u1, b1}
	for _, x := range arr2 {
		switch x.(type) {
		case Position:
			fmt.Println("derp")
		//case PositionI:
		//	fmt.Println("derp PositionI")
		//	fmt.Println("Position:", x.(PositionI).getPosition())
		case SpeedI:
			fmt.Println("derp SpeedI")
			fmt.Println("Speed:", x.(SpeedI).getSpeed())
		}
	}

	for _, x := range arr2 {
		fmt.Println("-> ", reflect.TypeOf(x))

		if pi, ok := x.(PositionI); ok {
			fmt.Println("X Position:", pi.getPosition())

		}
		if si, ok := x.(SpeedI); ok {
			fmt.Println("X Speed:", si.getSpeed())

		}

		if hasSpeed(x) {
			s := getSpeed(x)
			fmt.Println("derp:", s.speed)
		}
		s := getSpeed(x)
		fmt.Println("derp:", s.speed)
	}

	fmt.Println("Hello, playground", u1, b1, arr2)
}
