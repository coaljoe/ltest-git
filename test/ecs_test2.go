package main

// components
type Chimney struct {
}

func (c *Chimney) Render() {
	println("chimney smoking")
}

type Windows struct {
}

func (c *Windows) Render() {
	println("windows blinking")
}

type Building struct {
	Name string
}

func (b Building) Render() {
	println(b.Name, "render")
	println("don't know if has chimney or other components")
	/*
	  if b.Chimney != nil {
	    println("has chimney")
	  }
	  if b.Windows != nil {
	    println("has windows")
	  }
	*/
}

func (b Building) Destroy() {
	println("destroy", b.Name)
}

// entities
type Housing struct {
	*Building
	*Windows
	Components []interface{}
}

type Factory struct {
	*Building
	*Windows
	*Chimney
	Components []interface{}
}

func main() {
	b1 := &Housing{
		Building: &Building{Name: "Housing"},
		Windows:  &Windows{},
	}
	b1.Building.Render()

	b2 := &Factory{
		Building: &Building{Name: "Factory"},
		Windows:  &Windows{},
		Chimney:  &Chimney{},
	}
	b2.Building.Render()

	b1.Destroy()
	b2.Destroy()

	b2.Chimney.Render()
	b2.Windows.Render()

	// b1.Chimney.Render() // no chimney, fine error
	b1.Windows.Render()
}
