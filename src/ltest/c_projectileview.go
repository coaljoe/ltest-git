package main

// FxSys

import (
	"lib/ecs"
	"rx"
	//. "rx/math"
)

// Component
type ProjectileView struct {
	*View
	assetPath string
	m         *Projectile
	nodeName  string
}

func newProjectileView(en *ecs.Entity, m *Projectile) *ProjectileView {

	// Add view asset component
	//vac := newViewAsset(en, "res/objects/bullets/bullet")
	vac := newViewAsset(en, "res/objects/projectiles/"+m.ptype)
	vac.load()
	_ = vac

	v := &ProjectileView{
		View: newView(en),
		m:    m,
		//nodeName: "bullet",
		nodeName: "projectile",
	}
	en.AddComponent(v)
	return v
}

func (v *ProjectileView) load() {
	if v.loaded {
		return
	}
	println("ProjectileView.load()")
	ast := GetEntity(v).Get(ViewAssetT).(*ViewAsset).getAsset()
	v.node = ast.getNode(v.nodeName)
	v.loaded = true
}

func (v *ProjectileView) spawn() {
	println("ProjectileView.spawn()")
	if !v.loaded {
		v.load()
	}
	//v.node.spawn()
	rx.Rxi().Scene().Add(v.node)
	// Orientation
	v.node.LookAt2d(v.m.dPos)
	v.spawned = true
}

func (v *ProjectileView) delete() {
	println("ProjectileView.delete()")
	rx.Rxi().Scene().RemoveMeshNode(v.node)
	v.spawned = false
}

func (v *ProjectileView) update(dt float64) {
	newPos := v.m.Pos()
	//curPos := v.node.Pos()
	v.node.SetPos(newPos)
	//v.node.SetRot(v.m.Rot())

	// Fixme: can be called once?
	if v.m.Position.hasDPos() {
		v.node.LookAt2d(v.m.Position.dPos())
	}
}
