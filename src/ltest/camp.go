package main

import (
	"fmt"
)

type Camp struct {
	_id int    // Numerical id
	id  CampId // Camp identity
	// Resources
	fuel  *Resource
	ammo  *Resource
	money *Resource
	pop   *Resource
	// Link to camp's player
	player *Player
}

func newCamp(id CampId) *Camp {
	return &Camp{
		_id:   _ider.GetNextId("Camp"),
		id:    id,
		fuel:  newResource(ResourceType_Fuel),
		ammo:  newResource(ResourceType_Ammo),
		money: newResource(ResourceType_Money),
		pop:   newResource(ResourceType_Pop),
	}
}

func (c *Camp) name() string {
	return c.id.name()
}

/***** CampId *****/

type CampId int

const (
	CampId_Reds CampId = iota
	CampId_Suur
	CampId_Arctic
	CampId_None
	CampId_Len = iota // XXX: replace with MaxCampId?
)

func (ct CampId) name() string {
	switch ct {
	case CampId_Reds:
		return "Reds"
	case CampId_Suur:
		return "Suur"
	case CampId_Arctic:
		return "Arctic"
	case CampId_None:
		return "None"
	default:
		panic(fmt.Sprintf("unknown CampId ct=%v", ct))
	}
}

/*
func randomCampId() CampId {
	return CampId(rand.Intn(CampId_Len))
}
*/

func (ct CampId) String() string { return ct.name() }
