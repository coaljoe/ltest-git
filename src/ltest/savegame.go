package main

import (
	"bytes"
	"encoding/gob"
	//"encoding/json"
	"fmt"
	"github.com/kylelemons/godebug/pretty"
	"io/ioutil"
	//"lib/sr"
	"os"
	"reflect"
)

var _=`
func saveGame_(path string) {
	_log.Inf("Saving game to:", path)

	// Save game state
	mt := make(map[string]interface{})
	mt["Game"] = json.RawMessage(storeGame())
	mt["Timer"] = json.RawMessage(storeTimer(game.timer))
	pf("game timer ptr: %p\n", game.timer)
	fmt.Printf("game timer ptr: %p\n", game.timer)
	mt["TimerSys"] = json.RawMessage(storeTimerSys(game.timersys))

	js, err := json.MarshalIndent(mt, "", " ")
	if err != nil {
		panic(err)
	}
	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}
	//pp(string(js))
	_, err = f.WriteString(string(js))
	if err != nil {
		panic(err)
	}

	/*
		_, err = f.Write(js)
		if err != nil {
			panic(err)
		}
	*/
	if err := f.Sync(); err != nil {
		panic(err)
	}
	f.Close()
	_log.Inf("Saving complete")
}`

func saveGame(path string) {
	_log.Inf("Saving game to:", path)

	// Save game state

	// Gob test
	buf := &bytes.Buffer{}
	// Writing
	enc := gob.NewEncoder(buf)
	err := enc.Encode(game.timersys)
	if err != nil {
		panic(err)
	}
	err = enc.Encode(game.timersys)
	if err != nil {
		panic(err)
	}
	if true {
		err = enc.Encode(_ObjSys)
		if err != nil {
			panic(err)
		}
	}
	err = enc.Encode(game.getSystem("PositionSys").(*PositionSys))
	if err != nil {
		panic(err)
	}

	f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		panic(err)
	}
	//pp(string(js))
	_, err = f.Write(buf.Bytes())
	if err != nil {
		panic(err)
	}

	if err := f.Sync(); err != nil {
		panic(err)
	}
	f.Close()

	_log.Inf("Saving complete")
}

func loadGame(path string, verify bool) {
	_log.Inf("Loading game from:", path)

	if ok, err := exists(path); !ok {
		if err != nil {
			panic(err)
		}
		pp("file not found: ", path)
	}

	// Load game state

	dat, err := ioutil.ReadFile(path)
	check(err)

	// Reading
	//buf = bytes.NewBuffer(buf.Bytes())
	buf := bytes.NewBuffer(dat)
	dec := gob.NewDecoder(buf)

	// TimerSys
	_log.Inf("[Savegame] load timersys")
	//ts2 := newTimerSys() // XXX Returns singleton
	ts2 := &TimerSys{}
	dump(game.timersys)
	dump(ts2)
	if true { // Disables loading
		err := dec.Decode(ts2)
		if err != nil {
			panic(err)
		}
	}

	if verify {
		if !reflect.DeepEqual(game.timersys, ts2) {
			pp("not equal")
		}
	}

	// Replace
	game.timersys = ts2

	if true {

		// TimerSys
		_log.Inf("[Savegame] load timersys")
		//ts2 := newTimerSys() // XXX Returns singleton
		ts2 = &TimerSys{}
		dump(game.timersys)
		dump(ts2)
		if true { // Disables loading
			err := dec.Decode(ts2)
			if err != nil {
				panic(err)
			}
		}

		if verify {
			if !reflect.DeepEqual(game.timersys, ts2) {
				pp("TimerSys not equal")
			}
		}

		// Replace
		game.timersys = ts2

	}

	// ObjSys

	if true {
		_log.Inf("[Savegame] load ObjSys")
		
		os2 := &ObjSys{}
		dump(_ObjSys)
		dump(os2)
		err := dec.Decode(os2)
		if err != nil {
			panic(err)
		}
		//sr.Finalize(os2) // XXX Fixme
		dump(os2)

		p()
		p("deep dump:")
		p()
		dumpDepth(_ObjSys, 8)
		dumpDepth(os2, 8)

		if verify {
			if !reflect.DeepEqual(_ObjSys, os2) {
				fmt.Println(pretty.Compare(_ObjSys, os2))
				pp("ObjSys not equal")
			}
		}

		// Wipe current ObjSys (needed for testing)
		p("nullify objsys")
		_ObjSys.listElems()
		_ObjSys.clearElems()
		//_ObjSys = nil
		p("done")
		
		// Replace
		//_ObjSys = os2
		*_ObjSys = *os2
		/*
			_ObjSys.listElems()
			_ObjSys.clearElems()
			_ObjSys.listElems()
			pp(2)
		*/
		//pp(2)

		_log.Inf("Loading complete")
	}

	// PositionSys

	if true {
		_log.Inf("[Savegame] load PositionSys")

		ps := game.getSystem("PositionSys").(*PositionSys)
		ps2 := &PositionSys{}
		dump(ps)
		dump(ps2)
		
		err := dec.Decode(ps2)
		if err != nil {
			panic(err)
		}
		//sr.Finalize(ps2) // XXX Fixme
		p("ps2 dump:")
		dumpDepth(ps2, 8)
		p("ps elems size:", ps.size())
		p("ps2 elems size:", ps2.size())

		if verify {
			p("verify PositionSys:")
			if !reflect.DeepEqual(ps, ps2) {
				fmt.Println(pretty.Compare(ps, ps2))
				pp("PositionSys not equal")
			}
		}

		// Wipe current system (needed for testing)
		p("nullify PositionSys")
		ps.listElems()
		ps.clearElems()
		//ps = nil
		p("done")

		p("size before replacement:", game.getSystem("PositionSys").(*PositionSys).size())

		// Replace
		*ps = *ps2
		//ps = ps2 // XXX no replacement?
		//ps = nil
		//ps.clearElems()
		p("size after replacement:", game.getSystem("PositionSys").(*PositionSys).size())
		//pp(2)
		//pp(ps.size())

		_log.Inf("Loading complete")
	}

	_log.Inf("Load game complete")
}
