package main

import "lib/ecs"

func GetEntity(v interface{}) *ecs.Entity {
	//return ecs.GetEntity(v)
	//return v.(ecs.ComponentI).GetEntity()
	return _World.GetEntityById(v.(ecs.ComponentI).GetEntityId())
}

func c_Obj(en *ecs.Entity) *Obj {
	return en.Get(ObjT).(*Obj)
}

func c_Health(en *ecs.Entity) *Health {
	return en.Get(HealthT).(*Health)
}

func c_Combat(en *ecs.Entity) *Combat {
	return en.Get(CombatT).(*Combat)
}

func c_PlayerRecord(en *ecs.Entity) *PlayerRecord {
	return en.Get(PlayerRecordT).(*PlayerRecord)
}

func c_Unit(en *ecs.Entity) *Unit {
	return en.Get(UnitT).(*Unit)
}

func c_Building(en *ecs.Entity) *Building {
	return en.Get(BuildingT).(*Building)
}

func c_Turret(en *ecs.Entity) *Turret {
	return en.Get(TurretT).(*Turret)
}

//var c_Combat = getCombat
//var c_PlayerRecord = getPlayerRecord
