package main

import (
	"lib/ecs"
	"rx"
)

type ViewI interface {
	//Render()
	spawn()
	update(dt float64)
	delete()
	//GetMeshNodeByName(name string) *rx.MeshNode
	//GetEntity() *_Entity
	//GetEntity() *ecs.Entity
}

// Component?
type View struct {
	//*Component
	nodeName string
	ddir     string
	node     *rx.Node
	loaded,
	spawned bool
}

//func (vc *ViewCmp) NodeName() string { return vc.nodeName}
//func (vc *ViewCmp) GetMeshNode() *rx.MeshNode { return vc.node }

//func NewView(en *_Entity) *View {
func newView(en *ecs.Entity) *View {
	v := &View{}
	//v.Component = _NewComponent(en, v)
	return v
}

//func (v *View) GetEntity() *_Entity { return GetEntity(v) }

/*
func (v *View) Render() {
  for _, c := range v.Components() {
    cc := c.(ViewCmpI)
    cc.Render()
  }
}
*/

func (v *View) spawn() {
	panic("override")
}

func (v *View) delete() {
	panic("override") // Fixme
}

func (v *View) update(dt float64) {
	panic("override")
}

/* XXX inaccessible
func (v *View) GetMeshNodeByName(name string) *rx.MeshNode {
  for _, c := range v.Components() {
    cc := c.(ViewCmpI)
    nodeName := cc.NodeName()
    if nodeName == name {
      return cc.GetMeshNode()
    }
  }
  return nil
}
*/
