// Game extras.
package main

// Game run-time options.
// (a particular running game options/variables)
type GameOptions struct {
	dirty           bool    // GameOptions need update/apply
	_enableFogOfWar bool    // Read-only
	_gameSpeed      float64 // Read-only
}

func (_go *GameOptions) setGameSpeed(v float64) {
	_go._gameSpeed = v
	_go.dirty = true
}

func (_go *GameOptions) setEnableFogOfWar(v bool) {
	_go._enableFogOfWar = v
	_go.dirty = true
}

func newDefaultGameOptions() GameOptions {
	return GameOptions{
		_enableFogOfWar: true,
		_gameSpeed:      1.0, // Normal speed
		//_gameSpeed: 5.0,
	}
}

func newDebugGameOptions() GameOptions {
	gOpt := newDefaultGameOptions()
	gOpt._enableFogOfWar = false
	//gOpt._gameSpeed = 0.0 // Test
	gOpt.dirty = true
	return gOpt
}
