package main

import (
	"fmt"
	//. "math"
	"rx"
	//. "rx/math"
	rg "rx/rxgui"
)

type GuiMainSheet struct {
	*rg.Sheet
	topText   *rg.Label
	debugText *rg.Label
}

func newGuiMainSheet(g *Gui) *GuiMainSheet {
	s := &GuiMainSheet{
		Sheet: rg.NewSheet("MySheet"),
	}
	//g := _Gui
	g.rgi.SheetSys.AddSheet(s)
	g.rgi.SetEnabled(g.enabled)

	s.topText = rg.NewLabel(rg.Pos{0, 0}, "topText")
	//g.sheet.AddWidget(g.topText)
	s.Root().AddChild(s.topText)

	s.debugText = rg.NewLabel(rg.Pos{0, 0.97}, "debugText")
	s.debugText.SetVisible(false)
	s.Root().AddChild(s.debugText)

	return s
}

func (s *GuiMainSheet) Render(r *rx.Renderer) {
	// Call base
	s.Sheet.Render(r)

	//println("MYSHEET RENDER")
	//r := rxi.Renderer()

	r.RenderQuad(0.5, 0.0, 0.2, 0.1, 0)
	r.Set2DMode()
	rx.DrawLine(0, 0, 0, .5, .5, 0)
	//rx.DrawLine(0, 0, 0, 1, 1, 0)
	//rx.DrawLine(0, 0, 0, 100, 100, 0)
	r.Unset2DMode()

}

func (s *GuiMainSheet) Update(dt float64) {
	// Call base
	s.Sheet.Update(dt)

	//g.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d", g.cellX, g.cellY))
	s.topText.SetText(fmt.Sprintf("cellX: %d cellY: %d fps: %d",
		_Gui.cellX, _Gui.cellY, _rxi.App.Fps()))
}
