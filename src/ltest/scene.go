package main

import (
	//. "fmt"
	//. "math"
	"rx"
	. "rx/math"
)

type Scene struct {
	game     *Game
	camera   *Camera
	light    *rx.Node
	debugBox *rx.Node
}

func newScene(g *Game) *Scene {
	s := &Scene{
		game: g,
	}
	s._setDefaultScene()
	return s
}

// Create default scene.
func (s *Scene) _setDefaultScene() {
	s.camera = newCamera()
	s.camera.setDefaultCamera()

	rxi := rx.Rxi()
	sce := rxi.Scene()

	/* add light */

	l1 := sce.CreateLightNode("light1")
	println("l1 pos:", l1.Pos().String())

	s.light = l1

	/* add water */

	if true {
		sl := rx.NewSceneLoader()
		nodes := sl.Load("res/field/water/water.dae")
		waterMat := rx.NewMaterial("res/materials/water.json")
		waterMat.Alpha = true
		for _, n := range nodes {
			// Monkey path the texture
			tex := n.Mesh.Material().Texture()
			waterMat.SetTexture2(tex)
			// Set new texture
			n.Mesh.SetMaterial(waterMat)
			//n.Material().Alpha = true
			n.SetCulling(false)
			sce.Add(n)
		}
	}

	// Add debug box

	sl := rx.NewSceneLoader()
	nodes := sl.Load("res/models/primitives/box/box.dae")
	b := nodes[0]
	s.debugBox = b
	s.debugBox.SetVisible(false)
	sce.Add(s.debugBox)

	// Set culling properties
	//cullAABB: AABB{Vec3{-10, -10, -10}, Vec3{10, 10, 10}},
	//cullAABB: AABB{Vec3{-20, -20, -20}, Vec3{20, 20, 20}},
	//cullAABB: AABB{Vec3{-30, -30, -30}, Vec3{30, 30, 30}},
	//cullAABB: AABB{Vec3{-100, -100, -100}, Vec3{100, 100, 100}},
	//cullAABB: AABB{Vec3{-170, -150, -150}, Vec3{170, 150, 150}},
	//cullAABB: AABB{Vec3{-100, -100, -100}, Vec3{300, 300, 300}},

	aabb := rx.NewAABBFromPoints(Vec3{-170, -150, -150}, Vec3{170, 150, 150})
	s.camera.cam.Camera.SetLocalCullAABB(aabb)
}

func (s *Scene) update(dt float64) {
	s.camera.update(dt)
}
