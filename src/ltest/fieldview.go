package main

import (
	"fmt"

	"rx"
	. "rx/math"

	"github.com/go-gl/gl/v2.1/gl"
)

type FieldView struct {
	//*rx.Node
	m         *Field
	mns       []*rx.Node
	spawned   bool
	chunkSize int          // Size of a single chunk
	chunkMat  *rx.Material // Default chunk material
	fowView   *FowView
}

func newFieldView(m *Field) FieldView {
	v := FieldView{
		//Node: rx.NewNode(rx.NodeType_Custom),
		m:         m,
		mns:       make([]*rx.Node, 0),
		chunkSize: 16,
		//chunkSize: 12, // Smaller chunks are faster(?)
		//chunkSize: cell_size,
		//chunkSize: 8,
		fowView: newFowView(m),
	}
	//sub(rx.Ev_key_press, v.onKeyPress)
	return v
}

func (fv *FieldView) _buildChunk(chx, chy int) {
	_log.Dbg("[FieldView] buildChunk chx chy: ", chx, chy)
	m := fv.m
	//mapSize := fv.m.Size()
	//mapSize := fv.m.hm.w // square
	zoom := 1.0            // horizontal scale (default 1.0)
	scalef := float32(8.0) // vertical scale (default 4.0)
	//zoom = 0.25
	//zoom = 2
	zoom = 1.0
	chunkSeparateAmt := 0.0
	//chunkSeparateAmt := 1.0

	var points []float32
	/*
		for y := 0; y < fv.chunkSize-1; y++ {
			for x := 0; x < fv.chunkSize-1; x++ {
	*/
	for y := 0; y < fv.chunkSize; y++ {
		//for y := fv.chunkSize; y < 4; y-- {
		for x := 0; x < fv.chunkSize; x++ {
			//gl.Begin(gl.TRIANGLE_STRIP)

			// Step is one unit
			xf := float32(float64(x) * zoom)
			yf := float32(float64(-y) * zoom)
			zoomf := float32(zoom)

			/*
				gl.Vertex3f(xf, float32(m.HeightAt(i, j))*scalef, yf)
				gl.Vertex3f(xf+zoomf, float32(m.HeightAt(i+1, j))*scalef, yf)
				gl.Vertex3f(xf, float32(m.HeightAt(i, j+1))*scalef, yf+zoomf
				z			gl.Vertex3f(xf+zoomf, float32(m.HeightAt(i+1, j+1))*scalef, yf+zoomf)
			*/

			// HeightMap offsets
			hmX := chx*fv.chunkSize + x
			//hmY := chy*fv.chunkSize + y
			hmY := chy*fv.chunkSize + y
			//p("hmX,Y:", hmX, hmY)

			/*
				v1 := []float32{xf, float32(m.HeightMapHeightAt(hmY, hmX))*scalef, yf}
				v2 := []float32{xf+zoomf, float32(m.HeightMapHeightAt(hmY+1, hmX))*scalef, yf}
				v3 := []float32{xf, float32(m.HeightMapHeightAt(hmY, hmX+1))*scalef, yf+zoomf}
				v4 := []float32{xf+zoomf, float32(m.HeightMapHeightAt(hmY+1, hmX+1))*scalef, yf+zoomf}
			*/

			//v1 := []float32{xf, float32(m.XHeightMapHeightAt(hmX, hmY))*scalef, yf}
			//v2 := []float32{xf+zoomf, float32(m.XHeightMapHeightAt(hmX+1, hmY))*scalef, yf}
			//v3 := []float32{xf, float32(m.XHeightMapHeightAt(hmX, hmY+1))*scalef, yf+zoomf}
			//v4 := []float32{xf+zoomf, float32(m.XHeightMapHeightAt(hmX+1, hmY+1))*scalef, yf+zoomf}

			// Bottom Left
			v0 := []float32{xf, yf,
				//float32(m.getHmValueAtFlip(hmX, hmY)) * scalef}
				float32(m.getHmValueAt(hmX, hmY+1)) * scalef}
			// Bottom Right
			v1 := []float32{xf + zoomf, yf,
				//float32(m.getHmValueAtFlip(hmX+1, hmY)) * scalef}
				float32(m.getHmValueAt(hmX+1, hmY+1)) * scalef}
			// Top Left
			v2 := []float32{xf, yf + zoomf,
				//float32(m.getHmValueAtFlip(hmX, hmY-1)) * scalef}
				float32(m.getHmValueAt(hmX, hmY)) * scalef}
			// Top Right
			v3 := []float32{xf + zoomf, yf + zoomf,
				//float32(m.getHmValueAtFlip(hmX+1, hmY-1)) * scalef}
				float32(m.getHmValueAt(hmX+1, hmY)) * scalef}

			/*
				,	v1 := []float32{xf, float32(m.HeightMapHeightAt(i, j))*scalef, yf}
					v2 := []float32{xf+zoomf, float32(m.HeightMapHeightAt(i+1, j))*scalef, yf}
					v3 := []float32{xf, float32(m.HeightMapHeightAt(i, j+1))*scalef, yf+zoomf}
					v4 := []float32{xf+zoomf, float32(m.HeightMapHeightAt(i+1, j+1))*scalef, yf+zoomf}
			*/

			// triangle strip
			/*
				points = append(points, v1...)
				points = append(points, v2...)
				points = append(points, v3...)
				points = append(points, v4...)
			*/

			// Triangles

			// First
			points = append(points, v0...)
			points = append(points, v1...)
			points = append(points, v2...)
			// Second
			points = append(points, v2...)
			points = append(points, v1...)
			points = append(points, v3...)

			//gl.End()

		}
	}

	mb := rx.NewMeshBuffer()
	//mb.Add(points, nil, nil, nil)
	//p(points) // Fixme? does't work witout this
	//p(len(points))
	//mb.Add(points, []float32{}, []float32{}, []uint32{})
	//pointz := []float32{0.0, 0.0, 0.0, 10.0, 0.0, 0.0, 0.0, 10.0, 0.0}
	//pointz := make([]float32, len(points))
	//pointz = append(pointz, points...)
	//copy(pointz, points)
	//p("len pointz", len(pointz))
	//mb.Add(points, []float32{}, []float32{}, []uint32{})
	mb.Add(points[0:], []float32{}, []float32{}, []uint32{})

	mn := rx.NewMeshNode()
	mn.SetName(fmt.Sprintf("field_chunk_%d_%d", chx, chy))
	cacheId := fmt.Sprintf("field_chunk_%d_%d", chx, chy)
	//cachePath := fmt.Sprintf("_field_chunk_%s_%s", chx, chy)
	mn.Mesh.LoadFromMeshBuffer(mb, cacheId, "_")

	// Fixme:
	// Collada meshes matrix premultication in render hotfix
	//mn.SetRot(Vec3{-270, 0, 0})

	// Shift to buttom coord
	//fv.mn.SetPos(Vec3{0, 0, -float64(fv.m.h)*zoom})

	// Set chunk pos
	//mn.SetPos(Vec3{float64(chx*(fv.chunkSize-1))*zoom, 0,
	//    		  float64(chy*(fv.chunkSize-1))*zoom})
	//mn.SetPos(Vec3{float64(chx*(fv.chunkSize))*zoom, 0,
	//    		  float64(chy*(fv.chunkSize))*zoom})

	sepX := chunkSeparateAmt
	sepY := sepX
	if chx%2 == 0 {
		sepX = 0.0
	}
	if chy%2 == 0 {
		sepY = 0.0
	}

	// Warning: adjusting chunks z-pos at all levels,
	// Better use field's origin for this(?)
	zelev := zelev_ground0
	//mn.SetPos(Vec3{float64(chx*(fv.chunkSize)) * zoom,
	//	float64(chy*(fv.chunkSize)) * zoom, zelev})
	mn.SetPos(Vec3{float64(chx*(fv.chunkSize))*zoom + sepX,
		//float64(chy*(fv.chunkSize))*zoom + sepY,
		fv.m.origin.Y() - float64(chy*(fv.chunkSize))*zoom + sepY,
		zelev})
	//mn.SetRot(Vec3{0, 0, 90})

	// Set material
	mn.Mesh.SetMaterial(fv.chunkMat)

	fv.mns = append(fv.mns, mn)
}

func (fv *FieldView) _build() {
	_log.Dbg("[FieldView] build")
	//mapSize := fv.m.hm.w // Square
	//pp(mapSize, mapSize/ fv.chunkSize, fv.m.Size())

	//fv.buildChunk(0, 0)
	//fv.buildChunk(1, 0)
	//fv.buildChunk(7, 0)

	//targetChunkSize := 16
	//fv.chunkSize = mapSize / targetChunkSize
	//pp(fv.chunkSize)

	// Create material

	// Set material
	// Fixme: reuse material
	//mat := rx.NewMaterial(rx.ConfGetResPath() + "materials/static.json")
	mat := rx.NewMaterial("res/materials/field.json")
	mat.Name = "field"
	//mat.Name = fmt.Sprintf("field_mat_%s_%s", chx, chy)
	mat.Emission = Vec3Zero
	mat.Ambient = Vec3{0.2, 0.2, 0.2}
	mat.Diffuse = Vec3{1.0, 0.0, 1.0}
	mat.Specular = Vec3{1.0, 1.0, 1.0}
	mat.Hardness = 50.0
	fv.chunkMat = mat

	for y := 0; y < (fv.m.hm.h / fv.chunkSize); y++ {
		for x := 0; x < (fv.m.hm.w / fv.chunkSize); x++ {
			fv._buildChunk(x, y)
			//return
		}
	}

}

func (fv *FieldView) spawn() {
	// Update field origin
	//fv.m.origin = Vec3{0, float64(fv.m.h * fv.chunkSize), 0} // wrong
	fv.m.origin = Vec3{0, float64(fv.m.h * cell_size), 0}

	fv._build()
	for _, mn := range fv.mns {
		rx.Rxi().Scene().Add(mn)
	}
	//rx.Rxi().Scene().AddRenderable(fv)

	fv.fowView.spawn()
	fv.spawned = true
}

func (fv *FieldView) destroy() {
	for _, mn := range fv.mns {
		rx.Rxi().Scene().RemoveMeshNode(mn)
	}
	//rx.Rxi().Scene().RemoveRenderable(fv)
}

func (fv *FieldView) Render(r *rx.Renderer) {
	m := fv.m
	_ = m
	//pp(2)
	/*
			m := fv.m
			mapSize := fv.m.Size()
			zoom := 1.0
			scalef := float32(4.0)

			for i := 0; i < mapSize-1; i++ {
		  		for j := 0; j < mapSize-1; j++ {
		    		x := float64(i)*zoom
		    		y := float64(j)*zoom

		    		gl.Begin(gl.TRIANGLE_STRIP)

		    		xf, yf, zoomf := float32(x), float32(y), float32(zoom)

					gl.Vertex3f(xf, float32(m.HeightAt(i, j))*scalef, yf)
					gl.Vertex3f(xf+zoomf, float32(m.HeightAt(i+1, j))*scalef, yf)
					gl.Vertex3f(xf, float32(m.HeightAt(i, j+1))*scalef, yf+zoomf)
					gl.Vertex3f(xf+zoomf, float32(m.HeightAt(i+1, j+1))*scalef, yf+zoomf)

		    		gl.End()
		  		}
			}
	*/

	if vars.debugFieldGrid {
		//gl.Disable(gl.DEPTH_TEST)
		//gl.Disable(gl.CULL_FACE)

		//mapSize := fv.m.size() // Square

		// Draw field's origin
		oPos := m.origin
		gl.PushMatrix()
		gl.Translatef(float32(oPos.X()), float32(oPos.Y()),
			float32(oPos.Z()))
		gl.Scalef(10, 10, 10)
		rx.DrawAxes()
		gl.PopMatrix()

		// prepare
		gl.Color3f(0, 1, 1)
		//pp(2)

		// Draw downwards from field's origin
		for y := 0; y < m.h; y++ {
			for x := 0; x < m.w; x++ {
				//px := x * cell_size
				//py := x * cell_size
				px := float64(x * cell_size)
				py := m.origin.Y() - float64(y*cell_size)
				//pp(px, py)

				gl.PushMatrix()
				//gl.Translatef(float32(py), 0, float32(px))
				//gl.Translatef(float32(py), float32(px), 0)
				gl.Translatef(float32(py), float32(px),
					//float32(ZLevel_Ground0)+0.1)
					1.1)

				//rx.DrawAxes()
				//rx.DrawLine(0, 0, 0, 0, 0, 1)
				//rx.DrawDot(0, 0, 1)
				rx.DrawDot(0, 0, 0)

				/*
					if fv.m.IsCellPassable(i, j, CGroundRealm) {
						gl.Translatef(float32(cell_size/2), 0, float32(cell_size/2))
						rx.DrawAxes()
					}
				*/

				// Draw closed cells
				xx := x
				yy := (m.h - 1) - y // inverted y (in views?)
				if !m.isCellOpen(xx, yy) {
					gl.Translatef(-float32(cell_size/2), float32(cell_size/2), 0)
					rx.DrawAxes()
				}

				gl.PopMatrix()
			}
		}

		/*
			p("----")
			for y := 0; y < m.h; y++ {
				for x := 0; x < m.w; x++ {
					//yy := m.h - y
					//p("yy:", yy)
					c := m.cells[x][(m.h-1)-y]
					//c := m.cells[x][y]
					if !c.open {
						//if !m.isCellOpen(x, y) {
						print("#")
					} else {
						print(".")
					}
				}
				println()
			}
			p("----")
			pp()
		*/

		//gl.Enable(gl.CULL_FACE) // XXX: should be enabled by defaut in the renderer
		//gl.Enable(gl.DEPTH_TEST)
	}

	if vars.debugFieldZLevels {
		//gl.Disable(gl.DEPTH_TEST)
		//gl.Disable(gl.CULL_FACE)

		var colorMap map[ZLevel]Vec3
		colorMap = make(map[ZLevel]Vec3, 0)
		//step := 0.33
		//step1 := step
		//step2 := step * 2
		//step3 := step * 3
		colorMap[ZLevel_Sea0] = rx.ColorCyan
		colorMap[ZLevel_Sea1] = rx.ColorBlue
		colorMap[ZLevel_Sea2] = rx.ColorMagenta
		colorMap[ZLevel_Sea3] = rx.ColorBlack
		colorMap[ZLevel_Ground0] = Vec3{0, 1.0, 0}
		colorMap[ZLevel_Ground1] = rx.ColorYellow
		colorMap[ZLevel_Ground2] = rx.ColorRed
		colorMap[ZLevel_Ground3] = rx.ColorWhite
		//colorMap[ZLevel_Sea0] = Vec3{0, 0, 1.0}
		//colorMap[ZLevel_Sea1] = Vec3{0, step1, 1.0 - step1}
		//colorMap[ZLevel_Sea2] = Vec3{0, step2, 1.0 - step2}
		//colorMap[ZLevel_Sea3] = Vec3{0, step3, 1.0 - step3}
		//colorMap[ZLevel_Ground1] = Vec3{0, 1.0 - step1, 0}
		//colorMap[ZLevel_Ground2] = Vec3{0, 1.0 - step2, 0}
		//colorMap[ZLevel_Ground3] = Vec3{0, 1.0 - step3, 0}
		//p("hm:", m.heightMapHeightAt(0, 0), m.heightMapHeightAt(0, m.hm.h))
		p("hm:", m.heightMapHeightAt(1, 0), m.heightMapHeightAt(0, 1))
		p("hm:", m.getHmValueAt(0, 0), m.getHmValueAt(0, m.hm.h-1))
		p("hm:", m.getHmValueAt(1, 0), m.getHmValueAt(0, 1))
		//pp(m.getCellsAtZLevel(ZLevel_Sea3))
		//pp(2)

		/*
			    // Upward draw
				for x := 0; x < m.w; x++ {
					for y := 0; y < m.h; y++ {
						px := x * cell_size
						py := y * cell_size

						zlev := m.cells[x][y].z
						if v, ok := colorMap[zlev]; ok {
							rx.DebugDrawCube(
								Vec3{float64(px), float64(py), 0}, 2.0, v)
						} else {
							pp("zlev is not in colormap;", zlev)
						}

						//rx.DebugDrawCube(Vec3{float64(px), float64(py), 0}, 1.0,
						//colorMap[m.cells[x][y].z])
						//	rx.ColorRed)
					}
				}
		*/
		rx.DebugDrawCube(Vec3{0, 512, 0}, 5.0, Vec3{1, 0, 0})
		// Downward draw
		for y := 0; y < m.h; y++ {
			for x := 0; x < m.w; x++ {
				px := float64(x * cell_size)
				//py := y * cell_size
				//py := m.origin.Y() - float64(y*cell_size)
				py := m.origin.Y() - float64((y+1)*cell_size)
				//py := 500.0 - float64(y*cell_size)
				//pp(px, py)
				//yy := (m.h - 1) - y
				zlev := m.cells[x][y].z
				if v, ok := colorMap[zlev]; ok {
					//rx.DebugDrawCube(
					//	Vec3{px, py, 1}, 2.0, v)
					rx.DebugDrawPlane(
						Vec3{px + half_cell_size,
							py + half_cell_size, 1.1}, 4.0, v)
				} else {
					pp("zlev is not in colormap;", zlev)
				}

				//rx.DebugDrawCube(Vec3{float64(px), float64(py), 0}, 1.0,
				//colorMap[m.cells[x][y].z])
				//	rx.ColorRed)
			}
		}
	}
}

/*
func (fv *FieldView) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == glfw.KeyF9 {
		p("debugFieldGrind toggle")
		vars.debugFieldGrid = !vars.debugFieldGrid
	}
}
*/

func (fv *FieldView) update(dt float64) {
	if !fv.spawned {
		return
	}

	// Call render explicitly instead of using Renderable
	r := rx.Rxi().Renderer()
	fv.Render(r)

	//fv.Render()
	//p("herp")

	fv.fowView.update(dt)
}
