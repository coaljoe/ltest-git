package main

import (
	"fmt"
	"lib/ecs"
	ps "lib/pubsub"
	"rx"

	"github.com/go-gl/glfw/v3.1/glfw"
	//. "math"
)

var (
	_InputSys    *rx.InputSys
	_ObjSys      *ObjSys
	_ShedSys     *ShedSys
	_Scene       *Scene
	_PlayerSys   *PlayerSys
	_Field       *Field
	_UnitSys     *UnitSys
	_BuildingSys *BuildingSys
	_FowSys      *FowSys
	_Hud         *Hud
	_Gui         *Gui
	_Keymap      *Keymap
	game         *Game = nil
)

type Game struct {
	app       *rx.App
	gopt      GameOptions // Game Options
	systems   map[string]GameSystemI
	timer     *Timer
	frame     int
	scene     *Scene
	playersys *PlayerSys
	field     *Field
	world     *World
	inputsys  *rx.InputSys
	campsys   *CampSys
	unitsys   *UnitSys
	timersys  *TimerSys
	shedsys   *ShedSys
	fxsys     *FxSys
	hud       *Hud
	gui       *Gui
	// A game was created
	gameCreated bool
	paused      bool
	initialized bool
}

func newGame(app *rx.App) *Game {
	if game != nil {
		return game
	}

	println("game.new")
	//SetDefaultVars()
	g := &Game{
		frame: 0,
		app:   app,
		//gopt:    newDefaultGameOptions(),
		timer:       newTimer(true),
		systems:     make(map[string]GameSystemI),
		paused:      false,
		initialized: false,
	}
	game = g // Early linking

	// Set Game Options
	g.setGameOptions(newDefaultGameOptions())

	_ObjSys = newObjSys()
	g.inputsys = rx.NewInputSys(g.app)
	g.scene = newScene(g)
	g.playersys = newPlayerSys()
	g.field = _newField()
	g.world = newWorld()
	_BuildingSys = newBuildingSys()
	g.campsys = newCampSys()
	g.unitsys = newUnitSys()
	g.timersys = newTimerSys()
	g.shedsys = newShedSys()
	g.fxsys = newFxSys()
	g.hud = newHud()
	g.gui = newGui()
	_ = newPositionSys()
	_ = newHitSys()
	_Keymap = newKeymap()
	_ = newInputEx()
	_ = newFowSys()

	// XXX FIXME reapply game options for later systems (ugly)
	//g._updateGameOptions()

	// Set access vars
	_InputSys = g.inputsys
	_ShedSys = g.shedsys
	_Scene = g.scene
	_PlayerSys = g.playersys
	_Field = g.field
	_UnitSys = g.unitsys
	_FowSys = g.systems["FowSys"].(*FowSys)
	_Hud = g.hud
	_Gui = g.gui

	// Init complete
	g.initialized = true

	// Automatically start the game after creation (fixme)
	//g.start()

	sub(rx.Ev_key_press, g.onKeyPress)

	return game
}

func (g *Game) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == _Keymap.getKey1("pause") ||
		key == _Keymap.getKey2("pause") {
		// Toggle pause
		game.pause(!game.paused)
	}
}

func (g *Game) setGameOptions(gopt GameOptions) {
	_log.Inf("[Game] setGameOptions")

	g.gopt = gopt
	g._updateGameOptions()
}

func (g *Game) _updateGameOptions() {
	_log.Inf("[Game] _updateGameOptions")

	gOpt := &g.gopt // Shortcut
	// Update game speed
	if gOpt._gameSpeed >= 0.1 {
		g.timer.speed = gOpt._gameSpeed
	} else {
		pp("gameSpeed is too low:", gOpt._gameSpeed)
	}
	//pp(g.timer.speed, gOpt._gameSpeed)
	// Update fog of war
	//g.fogOfWar.enabled = gOpt._enableFogOfWar
	// XXX FIXME check systems readiness?
	// XXX Cannot be applyed, the field is generated/spawned AFTER
	// the game creation / initialization. Fixme?
	/*
		if _Field != nil && _Field.view.fowView != nil {
			_Field.view.fowView.setVisible(gOpt._enableFogOfWar)
			_Field.view.fowView.setVisible(false)
		}
	*/

	// Finally
	gOpt.dirty = false
}

func (g *Game) addSystem(name string, s GameSystemI) {
	//g.systems = append(g.systems, s)
	g.systems[name] = s
}

func (g *Game) getSystem(name string) GameSystemI {
	return g.systems[name]
}

func (g *Game) start() {
	//g._setDefaultGame()
	//g.hud.start()
	if !g.gameCreated {
		pp("no game was created")
	}

	// Start registered subsystems
	for _, si := range g.systems {
		if s, ok := si.(StartableI); ok {
			_log.Inf("[Game] start subsystem:", si.Name())
			s.start()
		}

	}
}

func (g *Game) pause(v bool) {
	//pp("not implemented")
	g.paused = v
}

func (g *Game) loadGame(path string) {
	// XXX load game state?
	// not whole game?
	loadGame(path, false)
}

func (g *Game) saveGame(path string) {
	saveGame(path)
}

// For testing, to save/load on the fly.
func (g *Game) saveload() {
	_log.Inf("[Game] saveload")
	path := vars.tmpDir + "/test.sav"
	//g.saveGame(path)
	//g.loadGame(path)
	saveGame(path)
	loadGame(path, true)
}

func (g *Game) clearGame() {
	for _, u := range _UnitSys.getUnits() {
		//_UnitSys.removeUnit(u)
		deleteEntity(u)
	}
	for _, ei := range _BuildingSys.getElems() {
		deleteEntity(ei.(*ecs.Entity))
	}
	sce := _rxi.Scene()
	_ = sce
	/*
		sce.RemoveMeshNode(sce.GetNodeById(1030))
		sce.RemoveMeshNode(sce.GetNodeById(4))
		sce.RemoveMeshNode(sce.GetNodeById(1029))
		sce.RemoveMeshNode(sce.GetNodeById(5))
		sce.RemoveMeshNode(sce.GetNodeById(3))
		sce.RemoveMeshNode(sce.GetNodeById(0))
		sce.RemoveMeshNode(sce.GetNodeById(2))
	*/
	fow := sce.GetNodeByName("fow_plane")
	sce.RemoveMeshNode(fow)
	//sce.ClearScene()
	//dump(_rxi.Scene())
	//pp(_rxi.Scene().GetNodes())
	//pp(_rxi.Renderer().CurCam())
	//pp(_rxi.Scene().GetNodes())
	if false {
		p(_rxi.Scene().GetVisibleNodes())
		n := _rxi.Scene().GetNodeById(1029)
		fmt.Printf("n: %#v\n", n)
		pdump(n)
		pp(2)
	}
}

//func game *Game { return gamei }

// Reset the game.
func (g *Game) _resetGame() {
	g.playersys.reset()
	g.gameCreated = false
}

// Create new default empty game.
func (g *Game) createNewDefaultGame() {
	g._resetGame()
	g._initBaseGame()
	g._initDefaultGame()
	// Finally
	g.gameCreated = true
}

// Create new test empty game.
func (g *Game) createNewTestGame() {
	g._resetGame()
	g._initBaseGame()
	g._initTestGame()
	// Finally
	g.gameCreated = true
}

// Init base game structure.
func (g *Game) _initBaseGame() {

	// Add player
	p := newPlayer(PlayerType_Human)
	p.name = "player1"
	g.playersys.addPlayer(p)

	// Add ai
	p2 := newPlayer(PlayerType_AI)
	p2.name = "ai1"
	g.playersys.addPlayer(p2)

	// Add ai2
	p3 := newPlayer(PlayerType_AI)
	p3.name = "ai2"
	g.playersys.addPlayer(p3)

	// Add none
	//p4 := newPlayer(PlayerType_AI)
	//p4.name = "none"
	//g.playersys.addPlayer(p4)

	// Make camps
	g.campsys.makeCamp(CampId_Reds, p)
	g.campsys.makeCamp(CampId_Suur, p2)
	g.campsys.makeCamp(CampId_Reds, p3) // Player1's ally(?)
	//g.campsys.makeCamp(CampId_None, p4) // None
	//g.campsys.makeCamp(CampId_Arctic, p3)

	//pdump(g.playersys.players)

	// Disable AIs
	p2.ai.disabled = true
	p3.ai.disabled = true

}

func (g *Game) _initDefaultGame() {
	// TODO: move field here from main
}

func (g *Game) _initTestGame() {
	// Set default field
	_log.Inf("[Game] generate default field...")
	g.field = _newField()
	//g.field.generateTest()

	f := g.field
	fsize := 16
	fw, fh := fsize, fsize
	hmW := fw * 2
	hmH := fh * 2
	//hmScale := 4.0
	//hmScale := 1.0
	//hmScale := 10.0
	hmOpt := newHeightMapOptions()
	fgOpt := newFieldGenerateOptions()
	fgOpt.disableEverything = false
	f.generate(fw, fh, hmW, hmH, hmOpt, fgOpt)

	_log.Inf("[Game] generate default field done")
}

func (g *Game) zero() bool {
	return g.frame == 0
}

func (g *Game) update(dt float64) {
	if game.paused {
		return
	}
	dt = dt * g.timer.speed

	// Update vars
	vars.dt = dt
	vars.dtGameSpeed = dt / g.gopt._gameSpeed

	// Update game options
	if g.gopt.dirty {
		g._updateGameOptions()
	}

	g.timersys.update(dt)
	g.shedsys.update(dt)
	g.field.update(dt)
	g.unitsys.update(dt)
	g.playersys.update(dt)
	g.fxsys.update(dt)
	g.scene.update(dt)
	//g.hud.update(dt)
	g.gui.update(dt)

	// Update registered systems
	for _, s := range g.systems {
		s.update(dt)
		//fmt.Println("Update", n, s)
	}
	//fmt.Println(g.systems)
	//g.hud.update(dt)

	g.timer.update(dt)
	g.frame += 1
}
