package main

import (
	"reflect"

	"lib/ecs"
)

//type EntityI interface {
//	Spawn()
//	//Render()
//	Update(dt float64)
//	GetName() string
//	Show()
//	GetView() ViewI
//	/*
//	  // Obj //
//	  Pos() Pos
//	  SetPos(p Pos)
//	  Rot() float64
//	  SetRot(r float64)
//	*/
//}

type DeleteableI interface {
	delete()
}

type UpdateableI interface {
	update(dt float64)
}

type SpawnableI interface {
	spawn()
}

func newEntity() *ecs.Entity {
	//pp("derp", _World, _World != nil)
	return _World.CreateEntity()
}

// Spawn entity.
// (fixme: move to custom entity system?)
func spawnEntity(en *ecs.Entity) {
	_log.Dbg("spawnEntity")
	for _, c := range en.Components() {
		switch c.(type) {
		case SpawnableI:
			xc := c.(SpawnableI)
			xc.spawn()
			//println("derp")
		}
	}
}

/*
// Doesn't work
func SpawnEntityBut(en *ecs.Entity, _c *ecs.Component) {
	for _, c := range en.Components() {
		if reflect.ValueOf(c) == reflect.ValueOf(_c) {
			continue
		}
		switch c.(type) {
		case SpawnableI:
			xc := c.(SpawnableI)
			xc.spawn()
			//println("derp")
		}
	}
}
*/

func deleteEntity(ent *ecs.Entity) {
	println("deleteEntity")
	///pp(1)
	if !_World.HasEntity(ent) {
		pp("deleteEntity failed: _World doesn't have this entity; deleting twice? id:", ent.Id())
	}
	ent.PrintComponents()
	/*
		// Call destructors on each component
		for _, c := range ent.Components() {
			switch c.(type) {
			case DeleteableI:
				xc := c.(DeleteableI)
				xc.delete()
			}
		}
	*/
	for _, c := range ent.Components() {
		if xc, ok := c.(DeleteableI); ok {
			p("calling delete on:", reflect.TypeOf(c))
			xc.delete()
		} else {
			p("not calling delete on:", reflect.TypeOf(c))
		}
	}
	ent.RemoveAllComponents()
	ent.PrintComponents()
	_World.RemoveEntity(ent)
	println("done")
	//ent.PrintComponents()
}

func updateEntity(ent *ecs.Entity, dt float64) {
	for _, c := range ent.Components() {
		//xc := c.(EntCompI)
		//xc.Update(dt)
		if xc, ok := c.(UpdateableI); ok {
			xc.update(dt)
		}
	}
}
