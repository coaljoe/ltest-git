// System contains building entities and handling functions.
package main

import (
	"lib/ecs"
	ps "lib/pubsub"
	. "rx/math"
)

type BuildingSys struct {
	*GameSystem
}

func newBuildingSys() *BuildingSys {
	s := &BuildingSys{}
	s.GameSystem = newGameSystem("BuildingSys", "Buildings system", s)
	//sub(ev_entity_destroy, s.onEntityDestroy)
	sub(ev_building_spawn, s.onBuildingSpawn)
	sub(ev_building_destroy, s.onBuildingDestroy)
	return s
}

/*
func (s *BuildingSys) onEntityDestroy(ev *ps.Event) {
	en := ev.Data.(*ecs.Entity)
	for u := range s.units {
		if u.Id() == en.Id() {
			// Fire ev_unit_destroy on this unit
			pub(ev_unit_destroy, u)
		}
	}
}
*/

func (s *BuildingSys) onBuildingSpawn(ev *ps.Event) {
	b := ev.Data.(*ecs.Entity)
	s.addElem(b)
}

func (s *BuildingSys) onBuildingDestroy(ev *ps.Event) {
	b := ev.Data.(*ecs.Entity)
	s.removeElem(b)
	// Call destructors
	deleteEntity(b)
	// Nullify building, fixme?
	b = nil
}

// Create and place building on the field.
func (s *BuildingSys) placeBuilding(cx, cy int, btype string, player *Player) bool {
	en := makeBuilding(btype, player)
	//c_Building(en).setCPos(h.cx, h.cy)
	pos := Vec3{0, 0, float64(ZLevel_Ground0)}
	p(pos)
	pos[0] = (float64(cx) * cell_size) + half_cell_size
	pos[1] = (float64(cy) * cell_size) + half_cell_size
	c_Building(en).SetPos(pos)
	spawnEntity(en)

	// Add to the system
	//s.addElem(en)

	return true
}

// Return building entities for player.
func (s *BuildingSys) getBuildingsForPlayer(p *Player) []*ecs.Entity {
	r := make([]*ecs.Entity, 0)
	for _, el := range s.getElems() {
		en := el.(*ecs.Entity) // XXX Slow?
		b := c_Building(en)
		if b.getPlayerRecord().player == p {
			r = append(r, en)
		}
	}
	return r
}

/*
func (bs *BuildingSys) listBuildings() {
	println("BuildingSys.ListBuildings")
	for i, b := range bm.buildings {
		println("-> ", i, "name:", b.name)
	}
}
*/

func (s *BuildingSys) update(dt float64) {
	for _, el := range s.getElems() {
		/*
			b := el.(*Building)
			b.update(dt)
		*/

		en := el.(*ecs.Entity)
		updateEntity(en, dt)
	}
}
