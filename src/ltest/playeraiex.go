package main

type PlayerAiOptions struct {
	airPreference    float64
	groundPreference float64
	navyPreference   float64
}

func newPlayerOptions() PlayerAiOptions {
	return PlayerAiOptions{
		airPreference:    0.33,
		groundPreference: 0.33,
		navyPreference:   0.33,
	}
}
