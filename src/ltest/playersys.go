package main

type PlayerSys struct {
	players []*Player
	player  *Player // Current player
}

func newPlayerSys() *PlayerSys {
	s := &PlayerSys{}
	s.reset()
	return s
}

func (s *PlayerSys) reset() {
	s.player = nil
	s.players = make([]*Player, 0)
}

// Get local human player.
func (s *PlayerSys) getPlayer() *Player {
	if s.player != nil {
		return s.player
	}
	for _, p := range s.players {
		if p.isHuman() {
			s.player = p
			return p
		}
	}
	panic("no human player?")
}

// Get player by name.
func (s *PlayerSys) getPlayerByName(name string) *Player {
	for _, p := range s.players {
		if p.name == name {
			return p
		}
	}
	pp("no player found; name:", name)
	panic(2)
}

func (s *PlayerSys) addPlayer(p *Player) {
	s.players = append(s.players, p)
}

func (s *PlayerSys) update(dt float64) {
	for _, p := range s.players {
		p.update(dt)
	}
}

// Get default local (current) player.
func getPlayer() *Player {
	return _PlayerSys.getPlayer()
}
