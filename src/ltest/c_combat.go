package main

import (
	. "math"
	"math/rand"
	"reflect"

	"lib/ecs"
)

var CombatT = &Combat{}

/*
type AttackableI interface {
	isAlive() bool
	//destroy()
	//Pos() Vec3
	pos2() Vec2     // Fixme?
	WorldPos() Vec3 // Fixme?
	getRealm() RealmType
	c_Health() int
	setHealth(amt int)
	defenceRating() int // Fixme: move?
	takeDamage(amt int)
	getTTHost() *TtTargetable
	c_Combat() *Combat
	c_PlayerRecord() *PlayerRecord
}
*/

// Component
type Combat struct {
	*ecs.Component
	//*WeaponsCmp
	// Links to weapon hosts
	whs []*WeaponHost
	//host AttackableI   // Fixme
	host *ecs.Entity
	// PursuitTarget? rename?
	//target AttackableI
	*TtTargetable
	/* props */
	//attackRadius  float64
	//attackTactics
	// ExperienceRating
	exp int
	// Attack rating
	//attack int
	// Defence rating
	defenceRating_ int
	// Armor rating
	//armorRating int
	/* state */
	//targetLocked bool
	locateTargetsLock bool
	deleted           bool // Fixme
}

func (c *Combat) getCombat() *Combat       { return c }
func (c *Combat) defenceRating() int       { return c.defenceRating_ }
func (c *Combat) getTtHost() *TtTargetable { return c.TtTargetable }

func newCombat(en *ecs.Entity) *Combat {
	c := &Combat{
		Component: ecs.NewComponent(),
		//WeaponsCmp: NewWeaponsCmp(),
		host: en,
		whs:  make([]*WeaponHost, 0),
		exp:  0,
		//target: nil,
		TtTargetable: newTtTargetable(),
	}
	en.AddComponent(c)
	return c
}

// Add link to weapon host.
func (c *Combat) addWeaponHost(wh *WeaponHost) {
	c.whs = append(c.whs, wh)
}

// Check if this component can perform any attacks.
func (c *Combat) canAttack() bool {
	return len(c.getWeapons()) > 0
}

/*
func (c *CombatCmp) AddHostedWeapon(w WeaponI, wh WeaponHost) {
  //c.WeaponsCmp.AddWeapon(w) // Bugfix: add this separately
  wh.SetLinkedWeapon(w)
  c.whs = append(c.whs, wh)
}
*/

// XXX: use with cation.
// Try set the target for all weapons.
func (c *Combat) setTarget(t *ecs.Entity) {
	//c.target = t
	// Fixme: replace to locate targets
	for _, wh := range c.whs {
		wh.setTarget(t)
	}
}

// Unset targets for all weapons.
func (c *Combat) unsetTargets() {
	for _, wh := range c.whs {
		wh.unsetTarget()
	}
}

func (c *Combat) getWeapons() (r []WeaponI) {
	for _, wh := range c.whs {
		r = append(r, wh.weapon)
	}
	return r
}

func (c *Combat) getTargets() (r []*ecs.Entity) {
	for _, wh := range c.whs {
		if wh.target != nil {
			r = append(r, wh.target)
		}
	}
	return r
}

// Get min-max attackRating range.
func (c *Combat) getAttackRatingRange() (int, int) {
	var minAr, maxAr int
	for _, w := range c.getWeapons() {
		ar := w.attackRating()
		if ar > maxAr {
			maxAr = ar
			if minAr == 0 {
				minAr = maxAr
			}
		} else {
			if ar < minAr {
				minAr = ar
			}
		}
	}
	return minAr, maxAr
}

/*
func (c *CombatCmp) CanAttack() bool {
  for _, wh := range c.whs {
    if !wh.IsTargetLocked() {
      return false
    }
  }
  return true
}
*/

func (c *Combat) _attackChecks(t *ecs.Entity) bool {
	host := c.host
	target := t

	// Same object
	if host == target {
		return false
	}

	// No player/camp on target
	if !c_PlayerRecord(target).hasPlayer() {
		return false
	}
	
	// Same camp
	if c_PlayerRecord(host).hasPlayer() && c_PlayerRecord(target).hasPlayer() &&
		c_PlayerRecord(host).player.camp.id == c_PlayerRecord(target).player.camp.id {
		return false
	}

	return true
}

func (c *Combat) locateTargets() {
	// Locate and set targets
	//println("Combat.LocateTargets")
	//r := c.MaxAttackRadius()
	r := c.getMaxLocateRadius()
	// FIXME: seems bug here

	p("derp:", c.host.ListComponents())
	objs := _ObjSys.getObjsInRadiusSortByDist(c_Obj(c.host).pos2(), r, false)

	for _, o := range objs {
		if !o.isActive() {
			// Skip inactive objects
			continue
		}
		any, ok := GetEntity(o).GetComponentCheck(CombatT)
		if ok {
			othc := any.(*Combat)
			target := othc.host
			if !c._attackChecks(target) {
				continue
			}
			for _, wh := range c.whs {
				if wh.canAttack(target) && !wh.hasTarget() {
					wh.setTarget(target)
				}
			}
			//c.SetTarget(othc.host)
		}
	}
}

func (c *Combat) attackTargets() {
	// Attack targets
	//println("attack target, IsAlive", c.target.IsAlive())
	for _, wh := range c.whs {
		//p(wh.weapon.CanFire())
		p("wh:", wh, "t:", wh.target)
		if wh.hasTarget() && wh.isTargetLocked() && wh.isTargetInRadius() &&
			wh.weapon.canFire() && c_Health(wh.target).isAlive() {
			// Check weapon is ready
			if wh.ready() && wh.weapon.ready() {
				//sleepsec(wh.weapon.AttackDelay())
				wh.fire()

				var _ = `
				bullet_speed := wh.weapon.ammoType().moveSpeed
				tt := distancePos2(wh.pos2(), wh.target.pos2()) / bullet_speed
				p("tt:", tt, wh.pos2(), wh.target.pos2())
				ar := wh.weapon.attackRating()
				target := wh.target
				/*
				   go func() {
				     sleepsec(tt)
				     if c != nil && target != nil {
				       p(c, ar, target)
				       c.performAttack(ar, target)
				     }
				   }()
				*/
				/*
				   p(c, ar, target)
				   c.performAttack(ar, target)
				*/
				_ = newShed(tt,
					func() {
						if c != nil && target != nil {
							p(c, ar, target)
							c._performAttack(ar, target)
						}
					})
				`
			}
		}
	}
}

// Максимальный радиус атаки из всех типов оружия, для Realm.
func (c *Combat) getAttackRadius(realm RealmType) float64 {
	r := 0.0
	for _, w := range c.getWeapons() {
		if w.getRealm() == realm {
			r = Max(r, w.radius())
		}
	}
	return r
}

// Максимальный радиус атаки из всех типов оружия.
func (c *Combat) getMaxAttackRadius() float64 {
	r := 0.0
	for _, w := range c.getWeapons() {
		r = Max(r, w.radius())
	}
	return r
}

// Максимальный радиус обнаружения из для всех типов оружия.
func (c *Combat) getMaxLocateRadius() float64 {
	r := 0.0
	for _, w := range c.getWeapons() {
		r = Max(r, w.locateRadius())
	}
	return r
}

// Calculate and apply damage to target.
func (c *Combat) _performAttack(attackRating int, target *ecs.Entity) {
	println("combat.performAttack")
	cknil(target)
	if target == nil || reflect.ValueOf(target).IsNil() {
		pdump(target)
	}
	ar := attackRating
	dr := c_Combat(target).defenceRating()
	res := ar - dr
	dmgPercent := 0.0
	dmg := 0.0

	// Расчет повреждений
	switch res {
	case -1:
		// Light failure
		dmgPercent = rand.Float64() * 1 // <1%
	case 0:
		// Unclear
		dmgPercent = rand.Float64() * 5 // <5%
	case 1:
		// Light success
		dmgPercent = 10.0 + rand.Float64()*40 // 10+%
	case 2:
		// Moderate success
		dmgPercent = 50.0 + rand.Float64()*40 // 50+%
	case 3:
		// Strong success
		dmgPercent = 90.0 + rand.Float64()*10 // 90+%
	}

	p("dmgPercent", dmgPercent, "res", res)
	p("health before", c_Health(target).health())
	curHealth := c_Health(target).health()
	//dmg = float64(c.target.GetHealth()) * (dmgPercent/100.0)
	maxHealth := 100.0 // Fixed, hp-less?
	dmg = maxHealth * (dmgPercent / 100.0)
	if dmg > 0 {
		c_Combat(target).takeDamage(int(dmg))
	}

	if (curHealth - int(dmg)) > 0 {
		p("health after", c_Health(target).health())
		//c.locateTargets()
	} else {
		p("health < 0")
		// Find new target immidietly fixme
		c.locateTargets()
	}
}

func (c *Combat) takeDamage(amt int) {
	h := c_Health(c.host)
	v := h.health() - amt
	h.setHealth(v)
	if !h.isAlive() {
		//c.host.destroy()
		//panic("fixme: not implemented")
		pub(ev_entity_destroy, GetEntity(c))
	}
}

func (c *Combat) listWeapons() {
	println("combat.listweapons()")
	for i, w := range c.getWeapons() {
		println("  ", i, w.typename())
	}
	println("end")
}

func (c *Combat) delete() {
	println("Combat.Delete")
	c.host = nil
	//c.target = nil
	for _, wh := range c.whs {
		wh.target = nil
		wh.host = nil
	}
	c.whs = nil
	// Clear all targets.
	c.unsetTargets()
	// Clear targetedBy's targets.
	c.TtTargetable.cleanTargetedBy()
	//c.unsetTargets()
	c.deleted = true

	//pp(c.getTargets())
}

func (c *Combat) update(dt float64) {
	if game.zero() || !c.locateTargetsLock {
		c.locateTargetsLock = true
		c.locateTargets()
		_ = newShed(vars.locateTargetsInterval,
			func() {
				if c != nil && !c.deleted { // Fixme, ugly
					cknil(c)
					if c.host == nil {
						dump(c)
						dump(GetEntity(c))
					}
					cknil(c.host)
					c.locateTargets()
					c.locateTargetsLock = false
				}
			})
	}
	if len(c.getTargets()) > 0 {
		c.attackTargets()
	}
	/*
	  if c.target != nil {
	    c.Attack()
	  }
	*/
}
