package main

import "lib/ecs"

//. "rx/math"

var ViewAssetT = &ViewAsset{}

type ViewAsset struct {
	*ecs.Component
	asset *Asset
	ddir  string
}

func newViewAsset(en *ecs.Entity, ddir string) *ViewAsset {
	t := &ViewAsset{
		Component: ecs.NewComponent(),
		asset: newAsset(),
		ddir:  ddir,
	}
	en.AddComponent(t)
	return t
}

func (vc *ViewAsset) load() {
	vc.asset.load(vc.ddir + "/model.dae")
}

func (vc *ViewAsset) getAsset() *Asset { return vc.asset }

func (vc *ViewAsset) update(dt float64) {}
func (vc *ViewAsset) spawn()            {}
func (vc *ViewAsset) render()           {}
func (vc *ViewAsset) delete() {
	println("fixme: ViewAsset.Delete not implemented")
}
