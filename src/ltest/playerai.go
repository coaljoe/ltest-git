package main

import (
	"lib/ecs"
)

// AI for a player.
type PlayerAi struct {
	p *Player
	// _Props_
	// Minimum set of buildings the camp should have
	requiredBuildings map[string]int
	// Logic
	logicUpdateTime  float64
	logicUpdateTimer *Timer
	disabled         bool
}

func newPlayerAi(p *Player) *PlayerAi {
	pai := &PlayerAi{
		p:                 p,
		requiredBuildings: make(map[string]int),
		logicUpdateTime:   2, //10,
		logicUpdateTimer:  newTimer(true),
		disabled:          false,
	}
	pai.requiredBuildings["factory"] = 1
	pai.requiredBuildings["housing"] = 3
	// Disable logger
	//_log.EnableFilterPrefix("[PlayerAi]")
	return pai
}

func (pai *PlayerAi) test() {

	if true {
		for y := 0; y < 4; y++ {
			for x := 0; x < 4; x++ {
				pai.placeBuilding(x, y, "bunker")
			}
		}
	}

	if true {
		ox, oy := 4, 4
		for y := 0; y < 4; y++ {
			for x := 0; x < 4; x++ {
				u := pai.placeUnit(ox+x, oy+y, "lighttank")
				_ = u
				//p(pai.p.name)
				//pp(c_Unit(u).getPlayer())
			}
		}
	}
}

func (pai *PlayerAi) updateMilitaryLogic() {
	// Find strongest player
	maxMilitaryScore := 0
	var topPlayer *Player

	//pp(len(_PlayerSys.players))
	for _, pl := range _PlayerSys.players {
		ms := pl.getMilitaryScore()
		p("military score for player:", pl, ms)
		if ms > maxMilitaryScore {
			maxMilitaryScore = ms
			topPlayer = pl
		}
	}

	p("Top player:", topPlayer, "Max military score:", maxMilitaryScore)

	// Military score for known units
	// XXX: in what cell are air units?
	knownUnitsMilitaryScore := 0.0
	knownUnitsCount := 0
	for _, pl := range _PlayerSys.players {
		mspu := pl.getMilitaryScorePerUnit()
		for id, ms := range mspu {
			u := _UnitSys.getUnitById(id)
			if _UnitSys.isUnitVisibleForPlayer(u, pai.p) {
				knownUnitsMilitaryScore += ms
				knownUnitsCount += 1
			}
		}
	}

	//pdump(pai.p.getFow())
	p("Known units military score:", knownUnitsMilitaryScore)
	p("Known units count:", knownUnitsCount)
	p("Total units count:", len(_UnitSys.getUnits()))
}

////////////////////////////////
// Micro management operations

// Pick a suitable place for a new building.
func (pai *PlayerAi) findAPlaceForBuilding(btype string) CPos {
	/*
		if ok := _BuildingSys.placeBuilding(cx, cy, btype, pai.p); !ok {
			pp("[PlayerAi] placeBuilding failed")
		}
	*/

	r := CPos{}
	return r
}

// Place building on behalf of Player
// XXX: should be in player class?
func (pai *PlayerAi) placeBuilding(cx, cy int, btype string) {
	cknil(_BuildingSys)
	cknil(pai)
	cknil(pai.p.camp)
	//pp(_BuildingSys)
	//pp(pai.p.camp.id)
	if ok := _BuildingSys.placeBuilding(cx, cy, btype, pai.p); !ok {
		pp("[PlayerAi] placeBuilding failed")
	}
}

// Place unit on behalf of Player
func (pai *PlayerAi) placeUnit(cx, cy int, utype string) *ecs.Entity {
	var u *ecs.Entity
	var ok bool
	//pp(pai.p.name)
	if ok, u = _UnitSys.placeUnit(cx, cy, utype, pai.p); !ok {
		pp("[PlayerAi] placeUnit failed")
	}
	return u
}

func (pai *PlayerAi) update(dt float64) {
	if pai.disabled {
		return
	}
	//pai.updateMilitaryLogic()
	if pai.logicUpdateTimer.dt() > pai.logicUpdateTime {
		//p._updateStats()
		pai.updateMilitaryLogic()
		pai.logicUpdateTimer.restart()
	}
}
