// Display local's player FoW
package main

import (
	"math/rand"
	"rx"
	. "rx/math"
)

var _ = Vec3Zero

type FowView struct {
	m       *Field
	mns     []*rx.Node
	fw, fh  int
	fx, fy  int
	spawned bool
	//chunkSize    int          // Size of a single chunk
	//chunkMat     *rx.Material // Default chunk material
	fowCellScale float64
	fow          *Fow
	fowTex       *rx.Texture
	origin       Vec3
	_visible     bool // Read-only
}

func newFowView(m *Field) *FowView {
	v := &FowView{
		m:   m,
		mns: make([]*rx.Node, 0),
		fx:  0,
		//fy:  -3,
		fy: 0,
		//fw: 16, //16,
		//fh: 16, //16,
		/*
			fw: 64, //16,
			fh: 64, //16,
		*/
		fw: m.w,
		fh: m.h,
		//chunkSize:    16,
		fowCellScale: 8.0,
		fow:          _FowSys.fow,
		origin:       Vec3Zero,
		_visible:     false,
	}

	if true {
		sl := rx.NewSceneLoader()
		nodes := sl.Load("res/field/fow/fow_mask.dae")
		//waterMat := rx.NewMaterial("res/materials/static.json")
		waterMat := rx.NewMaterial("res/materials/static1.json")
		//waterMat := rx.NewMaterial("res/materials/water.json")
		waterMat.Alpha = true
		_rxi.Scene().Add(nodes[0])
		for _, n := range nodes {
			// Monkey path the texture
			tex := n.Mesh.Material().Texture()
			// Set new texture
			n.Mesh.SetMaterial(waterMat)
			waterMat.SetTexture2(tex)
			tex.SetOpts(
				// Set nearest+mip filter
				rx.TextureOptions{
					MinFilter: rx.TextureFilterNearest,
					MagFilter: rx.TextureFilterNearest,
					Nomipmaps: false})
			//n.Material().Alpha = true
			//n.SetPosZ(4)
			n.SetPosZ(40)
			n.SetPosX(16)
			n.SetPosY(16)
			n.SetPosY(8)
			n.SetScale(Vec3One.MulScalar(256))
			n.SetPosX(256)
			//n.SetPosY(256)
			n.SetPosY(246)
			//pp("derp", n.Pos())
			//pp("derp")
			//_rxi.Scene().Add(n)
			n.SetCulling(false)
			v.fowTex = tex
		}
	}
	return v
}

func (v *FowView) _getTiles() MaskTiles {
	var tiles MaskTiles
	//tiles = v.fow.getRectSlice(v.fx, v.fy, v.fw, v.fh)
	tiles = v.fow.getFmask(v.fx, v.fy, v.fw, v.fh)
	//tiles = v.fow.getFmask(0, 0, v.fw, v.fh)
	//println(tiles.show())
	//pp("derp")
	return tiles
}

func (v *FowView) _build() {
	//m := _rxi.ResourceLoader.LoadScene("res/field/water/water.dae")[0]
	m := _rxi.ResourceLoader.LoadScene("res/field/fow/fow_tile.dae")[0]
	//_ = m
	c := game.scene.camera.cam
	cPos, cRot := c.Pos(), c.Rot()
	//m.SetRot(cRot)
	_, _ = cPos, cRot
	//m.SetPos(cPos)
	//	m.SetScale(Vec3One.MulScalar(10))
	//m.SetScale(Vec3One.MulScalar(8))
	m.SetScale(Vec3One.MulScalar(v.fowCellScale))
	//m.MoveByVec(Vec3Forward.MulScalar(-100))
	//m.MoveByVec(Vec3{0, 0, -100})
	//m.MoveByVec(Vec3{0, 0, -1})
	//m.MoveByVec(Vec3{0, 0, 10})
	m.MoveByVec(Vec3{0, 0, 3})
	//m.MoveByVec(Vec3Forward.MulScalar(1000))

	// Not adding the first chunk
	//_rxi.Scene().Add(m)
	//v.mns = append(v.mns, m)

	//m2 := m.Clone()
	//m2.MoveByVec(Vec3XUnit)
	//_rxi.Scene().Add(m2)

	for y := 0; y < v.fh; y++ {
		for x := 0; x < v.fw; x++ {
			m2 := m.Clone()
			_rxi.Scene().Add(m2)
			v.mns = append(v.mns, m2)
		}
	}

	v._placeTitles()

	v.refreshFows()
}

func (v *FowView) _placeTitles() {

	// Adjust XY positions by hand (fixme?)
	adjX, adjY := 1., -4.5 // Move to v.origin?
	for y := 0; y < v.fh; y++ {
		for x := 0; x < v.fw; x++ {
			px := float64(x) * v.fowCellScale
			py := float64(y) * v.fowCellScale
			//py := v.m.origin.Y() - float64(y)*v.fowCellScale
			//m2 := m.Clone()
			mn := v.mns[y*v.fh+x]
			mn.SetPosX(px + adjX)
			mn.SetPosY(py + adjY)
			fow := v.fow
			_ = fow
			//pdump(_UnitSys.getUnitsForPlayer(getPlayer()))
			//pdump(getPlayer().getFow)
			//pdump(v.m.fows[getPlayer()])
			//pdump(fow)
			//if fow.isCellVisible(x, y) {
			//if random(0, 1) > 0.5 {
			//	m2.SetVisible(false)
			//pp(x, y)
			//}
			_rxi.Scene().Add(mn)
			v.mns = append(v.mns, mn)
		}
	}
}

func (v *FowView) _hideTile(x, y int) {
	v.mns[y*v.fh+x].SetVisible(false)
}

func (v *FowView) _showTile(x, y int) {
	v.mns[y*v.fh+x].SetVisible(true)
}

// Disable fow in game.
func (v *FowView) hide() {
	//if !v.visible {
	//	return
	//}
	_log.Inf("[FowView] hide")
	for y := 0; y < v.fh; y++ {
		for x := 0; x < v.fw; x++ {
			v._hideTile(x, y)
		}
	}
	v._visible = false
}

// Enable fow in game.
func (v *FowView) show() {
	//if v.visible {
	//	return
	//}
	_log.Inf("[FowView] show")
	for y := 0; y < v.fh; y++ {
		for x := 0; x < v.fw; x++ {
			v._showTile(x, y)
		}
	}
	v.refreshFows()
	v._visible = true
}

func (v *FowView) setVisible(v_ bool) {
	if v_ == true {
		v.show()
	} else {
		v.hide()
	}
}

func (v *FowView) shift(dx, dy int) {
	_log.Inf("[FowView] shift;", dx, dy)
	v.fx += dx
	v.fy += dy
	v.refreshFows()
}

func (v *FowView) refreshFows() {
	// No point of doing this is not visible?
	if !v._visible {
		v.refreshFowTex()
		return
	}
	tiles := v._getTiles()
	for y := 0; y < v.fh; y++ {
		for x := 0; x < v.fw; x++ {
			//fow := v.m.fow
			//_ = fow
			//var fow *Fow
			//copy(fow, v.fow)
			//fow.mask = v._getTiles()

			//pdump(_UnitSys.getUnitsForPlayer(getPlayer()))
			//pdump(getPlayer().getFow)
			//pdump(v.m.fows[getPlayer()])
			//pdump(fow)
			//if v.fow.isCellVisible(x, y) {

			// Use flipped Y array/data repr. in views
			yInv := (v.fh - 1) - y
			tile := tiles[x][yInv] // Flipped
			//tile := tiles[x][y] // Not y-flipped

			if tile != true {
				//if random(0, 1) > 0.5 {
				//v.mns[y*v.m.h+x].SetVisible(false)
				v._hideTile(x, y)
				//pp(x, y)
			} else {
				v._showTile(x, y)
			}
		}
	}
}

func (v *FowView) refreshFowTex() {
	//return

	ix, iy := v.fw, v.fh
	//ix, iy := v.fow.w, v.fow.h
	image_data := make([]uint8, ix*iy*4)

	tiles := v._getTiles()
	idx := 0
	for y := 0; y < iy; y++ {
		for x := 0; x < ix; x++ {
			//println(x, y)
			//r, g, b, a := im.At(x, y).RGBA()
			//r, g, b, a := rand.Intn(255), rand.Intn(255), rand.Intn(255), 0
			//r, g, b, a := 0, 0, 255, 255
			//r, g, b, a := 0, 0, 255, 255
			r, g, b, a := 0, 0, 0, 255

			yInv := (v.fh - 1) - y
			tile := tiles[x][yInv] // Flipped
			//tile := tiles[x][y]
			_ = yInv

			if tile == false {
				a = 0
			}
			image_data[idx] = uint8(r)
			image_data[idx+1] = uint8(g)
			image_data[idx+2] = uint8(b)
			image_data[idx+3] = uint8(a) //uint8(255) //uint8(a)
			idx += 4
			_ = a
		}
	}

	data := image_data

	v.fowTex.SetTextureData(data)
}

func (v *FowView) _updatePos(dx, dy float64) {
	for _, mn := range v.mns {
		//mn.SetPos(Vec3{dx, dy, mn.PosZ()})
		//mn.SetPos(Vec3{dx, dy, mn.PosZ()})
		mn.AdjPos(Vec3{dx, dy, 0})
	}
}

func (v *FowView) spawn() {
	//v._build()
	v.spawned = true
}

func (v *FowView) update(dt float64) {
	if !v.spawned {
		return
	}
	//v.hide()

	// XXX: updated from gameoptions change/apply
	/*
		if game.gopt._enableFogOfWar && !v.visible {
			v.show()
		} else if !game.gopt._enableFogOfWar && v.visible {
			v.hide()
		}
	*/
	/*
		if !v.visible {
			v.hide()
		} else {
			v.show()
		}
	*/

	cam := game.scene.camera
	if cam.state == CameraState_Scrolling {
		//dx := cam.pos.X() - cam.prevPos.X()
		//dx := cam.pos.X() - v.m.origin.X()
		//dy := v.m.origin.Y() - cam.pos.Y()
		dx := cam.pos.X()
		dy := cam.pos.Y()
		//v.shift(1, 0)
		//fy := maxi(0, int(dx/v.fowCellScale))
		fx := maxi(0, int(dx/v.fowCellScale))
		fy := maxi(0, int(dy/v.fowCellScale))
		if fy+v.fh < v.fow.h && fx+v.fw < v.fow.w {
			v.fy = fy
			v.fx = fx
			//v.refreshFows()
		}
		//v.refreshFows()
		p("DELTA X:", dx, "Y:", dy)
		p("fx:", fx, "fy:", fy)

		cdx := cam.pos.X() - cam.prevPos.X()
		cdy := cam.pos.Y() - cam.prevPos.Y()
		_, _ = cdx, cdy
		//v._updatePos(cdx, cdy)
		//v._updatePos(cdx, cdy)
	}

	if false {
		//ix, iy := 64, 64
		ix, iy := v.fw, v.fh
		//ix, iy := v.fow.w, v.fow.h
		image_data := make([]uint8, ix*iy*4)

		// invert y-coord
		//for y := iy - 1; y >= 0; y-- {
		//	for x := 0; x < ix; x++ {

		idx := 0
		for y := 0; y < iy; y++ {
			for x := 0; x < ix; x++ {
				//println(x, y)
				//r, g, b, a := im.At(x, y).RGBA()
				//r, g, b, a := rand.Intn(255), rand.Intn(255), rand.Intn(255), 0
				r, g, b, a := rand.Intn(255), rand.Intn(255), rand.Intn(255), 255
				image_data[idx] = uint8(r)
				image_data[idx+1] = uint8(g)
				image_data[idx+2] = uint8(b)
				image_data[idx+3] = uint8(a) //uint8(255) //uint8(a)
				idx += 4
				_ = a
			}
		}
	}

	// Call render explicitly instead of using Renderable
	//r := rx.Rxi().Renderer()
	//_ = r
	//v.Render(r)

	//p("herp")
}
