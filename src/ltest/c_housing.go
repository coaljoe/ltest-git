package main

import "lib/ecs"

// Component
type Housing struct {
	pop *Resource
}

func newHousing(en *ecs.Entity) *Housing {
	h := &Housing{
		pop: newResource(ResourceType_Pop),
	}
	en.AddComponent(h)
	return h
}
