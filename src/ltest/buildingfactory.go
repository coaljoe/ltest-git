package main

//import . "fmt"

// Deprecated
var _ = `
func makeBuilding(btype string, camp CampId) *ecs.Entity {
	ent := newEntity()

	switch {

	// Housing
	case btype == "housing" && camp == CampId_Reds:
		b := newBuilding(ent)
		/* Building */
		b.name = "housing"
		_ = newHousing(ent)
		return ent

	// Factory
	case btype == "factory" && camp == CampId_Reds:
		b := newBuilding(ent)
		/* Building */
		b.name = "factory"
		// Add UnitFactoryCmp
		camp := newCamp(CampId_Reds)
		camp.money.amt = 1000
		_ = newUnitFactoryCmp(ent, camp)
		return ent

	// Airfield
	case btype == "airfield" && camp == CampId_Reds:
		b := newBuilding(ent)
		/* Building */
		b.name = btype
		_ = newAirfield(ent)
		return ent

	default:
		panic(Sprintf(
			"cannot make building, unknown settings: btype=%s camp=%d",
			btype, camp))
	}
}
`
