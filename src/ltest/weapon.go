// Непосредственно компонент оружия.
package main

type WeaponI interface {
	fire() bool
	reload()
	canFire() bool
	ready() bool
	//CanAttack(t AttackableI) bool
	typename() string
	getRealm() RealmType
	radius() float64
	locateRadius() float64
	attackRating() int
	attackDelay() float64
	ammoType() *AmmoType
	getShells() int
	setShells(v int)
	setDisabled(v bool)
}

/* abstract? */
// Wiki://Компоненты/Weapon
type Weapon struct {
	/* state */
	shells int
	/* props */
	fireRate  float64 // Shots/min
	maxShells int
	typename_ string
	//radius_, // Moved to AmmoType
	//locateRadius_ float64 // Moved to AmmoType
	maintanceCost float64 // Move to a component?
	//attackRating_ int // Moved to AmmoType
	attackDelay_ float64
	resuplyTime  float64
	ammo         *Resource
	ammoType_    *AmmoType
	realm        RealmType
	//autoReload bool
	//attackDelayTimer *Timer
	fireIntervalTimer *Timer
	disabled          bool
}

//func (w *Weapon) radius() float64       { return cellsToUnits(w.ammoType_.affectRadius) }
func (w *Weapon) typename() string      { return w.typename_ }
func (w *Weapon) getRealm() RealmType   { return w.realm }
func (w *Weapon) radius() float64       { return w.ammoType_.radius }
func (w *Weapon) locateRadius() float64 { return w.ammoType_.locateRadius }
func (w *Weapon) attackRating() int     { return w.ammoType_.attackRating }
func (w *Weapon) attackDelay() float64  { return w.attackDelay_ }
func (w *Weapon) ammoType() *AmmoType   { return w.ammoType_ }
func (w *Weapon) getShells() int        { return w.shells }
func (w *Weapon) setShells(v int)       { w.shells = v }
func (w *Weapon) setDisabled(v bool)    { w.disabled = v }

func newWeapon() *Weapon {
	w := &Weapon{
		shells:    0,
		maxShells: 0,
		fireRate:  60, // Shots/min
		ammo:      newResource(ResourceType_Ammo),
		ammoType_: newAmmoType(),
		realm:     RealmType_Ground,
		//attackDelayTimer: NewTimer(),
		fireIntervalTimer: newTimer(true),
		disabled:          false,
	}
	return w
}

func (w *Weapon) canFire() bool {
	if w.disabled {
		return false
	}
	if w.shells > 0 {
		return true
	} else {
		return false
	}
}

func (w *Weapon) ready() bool {
	if w.fireIntervalTimer.dt() < w.fireInterval() {
		//println("waiting for fire interval")
		return false
	}
	return true
}

func (w *Weapon) fire() bool {
	if !w.canFire() {
		println("cant fire")
		return false
	}
	w.shells -= 1
	println("Weapon: shell left:", w.shells)
	// Autoreload
	if w.shells < 1 {
		if w.hasAmmo() {
			w.reload()
		} else {
			println("autoreload: no ammo left")
		}
	}
	w.fireIntervalTimer.restart()
	return true
}

func (w *Weapon) reload() {
	println("Weapon: reload()")
	/*
	  go func() {
	    sleepsec(w.resuplyTime)
	    if w != nil && w.ammo != nil {
	      p(w.ammoType.weight, w.maxShells, w.ammo.amt)
	      w.shells += w.ammo.TakeByUnits(w.maxShells, w.ammoType.weight)
	      p(w.ammoType.weight, w.maxShells, w.ammo.amt)
	      p(w.shells)
	    }
	  }()
	*/
	/*
	  p(w.ammoType.weight, w.maxShells, w.ammo.amt)
	  w.shells += w.ammo.TakeByUnits(w.maxShells, w.ammoType.weight)
	  p(w.ammoType.weight, w.maxShells, w.ammo.amt)
	  p(w.shells)
	*/
	_ = newShed(w.resuplyTime,
		func() {
			if w != nil && w.ammo != nil {
				p(w.ammoType_.weight, w.maxShells, w.ammo.amt)
				w.shells += w.ammo.takeByUnits(w.maxShells, w.ammoType_.weight)
				p(w.ammoType_.weight, w.maxShells, w.ammo.amt)
				p(w.shells)
			}
		})
}

func (w *Weapon) hasAmmo() bool {
	return w.ammo.amt > 0
}

// Fire interval in seconds
func (w *Weapon) fireInterval() float64 {
	return 1.0 / (w.fireRate / 60.0)
}

/*
func (w *Weapon) AddAmmo(n float64) {
  w.shells += w.Ammo.TakeByUnits(w.maxShells, w.shellWeight)
}
*/
