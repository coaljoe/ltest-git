// Additional input processing.
package main

import (
	"fmt"
	"rx"
	"time"

	ps "lib/pubsub"

	"github.com/go-gl/glfw/v3.1/glfw"
)

type InputEx struct {
	*GameSystem
	f9DebugModes  []string
	f9DebugModeId int
}

func newInputEx() *InputEx {
	ie := &InputEx{
		f9DebugModes:  []string{"none", "grid", "pathfinding", "zlevels"},
		f9DebugModeId: 0,
	}
	ie.GameSystem = newGameSystem("InputEx", "InputEx subsystem", ie)
	sub(rx.Ev_key_press, ie.onKeyPress)

	return ie
}

func (ie *InputEx) onKeyPress(ev *ps.Event) {
	key := ev.Data.(glfw.Key)
	if key == glfw.KeyF9 {
		// Cycle trough modes
		ie.f9DebugModeId = (ie.f9DebugModeId + 1) % len(ie.f9DebugModes)
		debugMode := ie.f9DebugModes[ie.f9DebugModeId]

		switch debugMode {
		case "none":
			{
				_log.Inf("[InputEx] debugMode: none")
				// Unset prev
				{
					vars.debugFieldZLevels = !vars.debugFieldZLevels
				}

				println("none")
			}
		case "grid":
			{
				_log.Inf("[InputEx] debugMode: grid")
				p("debugFieldGrind toggle")
				vars.debugFieldGrid = !vars.debugFieldGrid
			}
		case "pathfinding":
			{
				_log.Inf("[InputEx] debugMode: pathfinding")
				// Unset prev
				{
					vars.debugFieldGrid = !vars.debugFieldGrid
				}

				// Set new
				vars.debugPathfinding = !vars.debugPathfinding
			}
		case "zlevels":
			{
				_log.Inf("[InputEx] debugMode: zlevels")
				p("debugFieldZLevels toggle")
				// Unset prev
				{
					vars.debugPathfinding = !vars.debugPathfinding
				}

				// Set new
				vars.debugFieldZLevels = !vars.debugFieldZLevels
			}
		}
	}

	if key == glfw.KeyF1 {
		game.clearGame()
	}
	if key == glfw.KeyF2 || key == glfw.KeyF { // XXX doesn't work in colemak
		//pp("derp")
		// Toggle fog
		//game.gopt.setEnableFogOfWar(!game.gopt._enableFogOfWar)
		v := _Field.view.fowView._visible
		_Field.view.fowView.setVisible(!v)
	}
	if key == glfw.KeyF3 {
		game.saveload()
	}
	if key == glfw.KeyF4 {
		game.saveGame(vars.tmpDir + "/test.sav")
	}
	if key == glfw.KeyF5 {
		game.loadGame(vars.tmpDir + "/test.sav")
		game.pause(true)
	}
	//if key == glfw.KeyF12 && _InputSys.Keyboard.IsKeyPressed(glfw.KeyRightControl) {
	if key == glfw.KeyF6 {
		//pp(2)
		ts := int32(time.Now().Unix())
		path := fmt.Sprintf("ss %s %s %d.png", AppName, BuildRevision, ts)
		ok := rx.SaveScreenshot(path)
		if !ok {
			_log.Err("Screenshot save fail; path=", path)
		} else {
			_log.Inf("Saved screenshot to:", path)
		}
	}
}

func (ie *InputEx) update(dt float64) {

}
