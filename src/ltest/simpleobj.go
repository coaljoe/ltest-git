package main

import "fmt"

// A simplyfied object type without ecs.Entity and
// Other subsystems use.
type SimpleObj struct {
	id int
	*Transform
}

func newSimpleObj() *SimpleObj {
	return &SimpleObj{
		id:        _ider.GetNextId("SimpleObj"),
		Transform: newTransform(),
	}
}

func (o *SimpleObj) String() string {
	return fmt.Sprintf("SimpleObj<id=%d>", o.id)
}
