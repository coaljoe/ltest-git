package main

import (
	"lib/ecs"
	. "rx/math"
)

type BuildingI interface {
	Render()
	Show()
}

// For all buildings.
//component
type Building struct {
	*ecs.Component
	*Obj
	*Health       // Link?
	*PlayerRecord // Link
	*Combat       // Link
	realm         RealmType
	/*
	*UnitFactoryCmp // Optional
	*Housing        // Optional
	*Airfield       // Optional
	 */
	id       int
	name     string // Const
	typename string // Const
	btype    string
	// The amount of space the building takes (W, H)
	cellSpace Vec2
	cellPos   CPos
	// Related static unit
	staticUnit *Unit
	spawned    bool
	View       ViewI
	//dirty     bool
	//dirtyView bool
}

func (b *Building) getRealm() RealmType { return b.realm }

func newBuilding(en *ecs.Entity) *Building {
	b := &Building{
		Component: ecs.NewComponent(),
		id:        _ider.GetNextId("building"),
		Obj:       newObj(en, "building obj"),
		cellSpace: Vec2{1, 1},
		//Obj:    NewObj(ent, "unnamed"),
		//Health: newHealth(100),
		//View: NewBuildingView(), // Default
		//UnitFactoryCmp: nil,
		realm: RealmType_Ground,
	}
	en.AddComponent(b)
	b.Health = newHealth(en, 100)
	b.PlayerRecord = newPlayerRecord(en)
	b.Combat = newCombat(en)
	return b
}

func (b *Building) _build() {
	/*
		if !b.dirty {
			return
		}
	*/

	// Update cellPos
	b.cellPos.x = int(b.PosX() / cell_size)
	b.cellPos.y = int(b.PosY() / cell_size)

	//b.dirty = false
}

// Fixme: rename to OnSpawn?
// XXX cannot be called directly, use _entity.spawn?
func (b *Building) spawn() {
	//pp(2)
	if b.spawned {
		return
	}
	b.spawned = true // Must be set before SpawnEntity

	println("Building.spawn; id = ", b.id, b.spawned)
	// XXX only sending an event from here
	pub(ev_building_spawn, GetEntity(b))
	//SpawnEntityBut(GetEntity(u), u.Component)
	spawnEntity(GetEntity(b))

	//u.View.Spawn()
	b.View.spawn()
}

func (b *Building) destroy() {
	// Explode
	println("Building.destroy")
	pub(ev_building_destroy, GetEntity(b))
}

func (b *Building) delete() {
	_log.Inf("Building.delete")
	_BuildingSys.removeElem(GetEntity(b))
}

func (b *Building) show() {
	println("Building.show\n name:", b.name)
}

// Place building at center of pos x, y.
func (b *Building) setCPos(x, y int) {
	adj := cell_size / 2
	b.setPos2(Vec2{float64(x*cell_size + adj), float64(y*cell_size + adj)})
	//b.cellPos = CPos{x, y}
	b._build() // FIXME?
}

func (b *Building) update(dt float64) {
	b._build()
}
