package main

import (
	"lib/ecs"
	ps "lib/pubsub"
	//. "rx/math"
)

type FxSys struct {
	*GameSystem
}

func newFxSys() *FxSys {
	s := &FxSys{}
	s.GameSystem = newGameSystem("FxSys", "Fx System", s)
	sub(ev_weaponhost_fire, s.onWeaponHostFire)
	sub(ev_projectile_hit, s.onProjectileHit)
	return s
}

// Fixme: move to projectilesys?
func (s *FxSys) onWeaponHostFire(ev *ps.Event) {
	wh := ev.Data.(*WeaponHost)
	// Fixme: findViewByComponent/Name ?
	//uc := c_Combat(wh.host).Get(UnitT).(*Unit)
	uc := c_Unit(wh.host)
	//en := wh.host.Entity
	//firepoint_node_name := "turret_firepoint"
	v := uc.getView()
	tvc := GetEntity(v).Get(TurretViewT).(*TurretView)
	sPos := tvc.firepointNode.WorldPos()
	pos := c_Obj(wh.target).pos2() // Fixme
	dPos := pos.ToVec3()
	speed := wh.weapon.ammoType().moveSpeed //50.0
	//speed := 5.

	p := makeProjectile(sPos, dPos, speed, 0, wh.weapon.ammoType(), GetEntity(v), wh.target)
	spawnEntity(p)
}

func (s *FxSys) onProjectileHit(ev *ps.Event) {
	e := ev.Data.(EvProjectileHit)
	_ = e
	for i := 0; i < 360; i += 10 {
		// Do nothing
	}
}

/*
func (s *FxSys) sendProjectile(from, to Vec3, speed float64) {
	p := makeProjectileEntity(from, to, speed, 0)
	spawnEntity(p)
}
*/

func (s *FxSys) update(dt float64) {
	for _, xi := range s.getElems() {
		en := xi.(*ecs.Entity)
		updateEntity(en, dt)
	}
	//fs.bulletsys.Update(dt)
}
