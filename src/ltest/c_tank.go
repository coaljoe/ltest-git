package main

//. "math"
//. "rx/math"
import "lib/ecs"

// Component
type Tank struct {
	*ecs.Component
	mt *Turret
}

func newTank(en *ecs.Entity) *Tank {
	t := &Tank{
		Component: ecs.NewComponent(),
	}
	en.AddComponent(t)
	//en.PrintComponents()
	t.mt = en.GetComponentTag(&Turret{}, "main").(*Turret)
	return t
}

func (tc *Tank) delete() {
	println("Tank.Delete")
	tc.mt = nil
}

func (tc *Tank) update(dt float64) {
	//p("Tank.Update")
	//mt := tc.GetComponentTag(&Turret{}, "main").(*Turret)
}
