package main

//"github.com/bpicode/fritzctl/assert"

type FieldGenerateOptions struct {
	// Generate anything from these options at all
	disableEverything bool
	// HM times bigger than the field (fixme: can be calculated?)
	hmScale              int
	generateFieldObjects bool
	generateOilRigs      bool
	amountFieldObjects   float64
	amountOilRigs        float64
}

func newFieldGenerateOptions() FieldGenerateOptions {
	return FieldGenerateOptions{
		disableEverything: false,
		// Default HM is X times larger than field size
		hmScale:              4,
		generateFieldObjects: true,
		generateOilRigs:      false,
		amountFieldObjects:   1.0,
		amountOilRigs:        1.0,
	}
}

// Verify options for correctness.
func (fgo FieldGenerateOptions) verify() {
	// Skip test
	if fgo.disableEverything {
		return
	}
	if fgo.hmScale < 1 {
		pp("hmScale is less than 1;", fgo.hmScale)
	}
	//assert.IsTrue(fgo.hmScale > 1,
	//	"hmScale is less than 1;", fgo.hmScale)
}

func generateFieldObjects(f *Field, fgOpt FieldGenerateOptions) {
	_log.Inf("[Fieldgen] generate field objects...")
	if fgOpt.disableEverything {
		//pp("derp")
		return
	}

	// XXX temporary hack
	fw := f.w / 2
	fh := f.h / 2

	// Add field objects
	if fgOpt.generateFieldObjects {
		for x := 0; x < fw; x++ {
			for y := 0; y < fh; y++ {
				cell := &f.cells[x][y]
				if !cell.z.isGround() {
					continue
				}
				if random(0, 1) > 0.9 {
					//if true {
					fo := newFieldObject("tree")
					fo.SetPosX(float64(x*cell_size + half_cell_size))
					fo.SetPosY(float64(y*cell_size + half_cell_size))
					// Set random orientation
					fo.SetRotZ(random(0, 360))
					f.objects = append(f.objects, fo)
					//pp("derp")
				}
			}
		}
	}

	// Add oil rigs
	if fgOpt.generateOilRigs {
		oilRigCells := make([]Cell, 0)
		oilRigLocs := make([]CPos, 0)
		_ = oilRigCells
		_ = oilRigLocs

		// Props
		siteW, siteH := 3, 3
		//zlev := ZLevel_Sea1
		zlev := ZLevel_SeaMax + 1

		//pp(f.getCellsAtZLevel(zlev))
		//pp(f.getCellsAtZLevel(ZLevel_Ground0))
		//pp(f.getCellsAtZLevel(ZLevel_Sea1))
		//pp(f.getCellsAtZLevel(ZLevel(-1)))
		//pp(f.getCellsAtZLevels(ZLevel_Sea0, ZLevel_SeaMax))
		//pp(f.getCellsAtZLevels(ZLevel_SeaMax, ZLevel_Sea0))
		//pp(f.getCells())

		// Find suitable places for oilrigs
		for x := 0; x < f.w-siteW; x++ {
			for y := 0; y < f.h-siteH; y++ {
				//oilRigLocs = append(
				cell := &f.cells[x][y]
				if cell.z != zlev {
					continue
				}
				p(cell)

				suitable := true

				nb := f.getRectSlice(x, y, siteW, siteH)

				for _, n := range nb {
					if n.z != zlev {
						suitable = false
					}
				}

				if suitable {
					oilRigLocs = append(oilRigLocs, CPos{x, y})
				}
			}
		}

		//pp(oilRigLocs)

		for _, p := range oilRigLocs {
			x, y := p.x, p.y
			fo := newFieldObject("tree")
			fo.SetPosX(float64(x*cell_size/2 + half_cell_size/2))
			fo.SetPosY(float64(y*cell_size/2 + half_cell_size/2))
			//fo.setXRot(10)
			f.objects = append(f.objects, fo)
		}
	}

	_log.Inf("[Fieldgen] generate field objects complete")
}

func generateFieldZones(f *Field, fgOpt FieldGenerateOptions) {
	_log.Inf("[Fieldgen] generate field zones...")
	if fgOpt.disableEverything {
		return
	}

	// Generate camp sites
	for _, pl := range game.playersys.players {
		_log.Inf("[Fieldgen] generate camp site for player:", pl.name)
		zlev := ZLevel_Ground0
		minCells := 40 // Continuous cells
		_, _ = zlev, minCells
	}

	_log.Inf("[Fieldgen] generate field zones complete")
}
