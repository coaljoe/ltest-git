// A system for managing Fog of war in game.
package main

import (
	"lib/ecs"
	ps "lib/pubsub"
)

type FowSys struct {
	*GameSystem
	fows map[*Player]*Fow
	fow  *Fow // Link to player's fow
}

func newFowSys() *FowSys {
	s := &FowSys{
		fows: make(map[*Player]*Fow, 0),
	}
	s.GameSystem = newGameSystem("FowSys", "Fog of war system", s)
	// Events
	sub(ev_unit_spawn, s.onUnitSpawn)
	sub(ev_unit_step, s.onUnitStep)
	return s
}

func (s *FowSys) onUnitSpawn(ev *ps.Event) {
	u := ev.Data.(*ecs.Entity)
	uc := c_Unit(u)
	cx, cy := uc.cpos().x, uc.cpos().y
	s.updateFowForUnit(u, cx, cy)
	if uc.getPlayer().isDefaultPlayer() {
		_Field.view.fowView.refreshFows()
	}
}

func (s *FowSys) onUnitStep(ev *ps.Event) {
	d := ev.Data.(EvUnitStep)
	s.updateFowForUnit(d.unit, d.dCx, d.dCy)
	uc := c_Unit(d.unit)
	if uc.getPlayer().isDefaultPlayer() {
		_Field.view.fowView.refreshFows()
	}
}

func (s *FowSys) generateFows(w, h int) {
	// Add fows
	cknil(game.playersys)
	for _, pl := range game.playersys.players {
		s.fows[pl] = newFow(w, h)
		//if pl == getPlayer() {
		if pl.isDefaultPlayer() {
			s.fow = s.fows[pl]
		}
	}
}

func (s *FowSys) getFowForPlayer(player *Player) *Fow {
	fow, ok := s.fows[player]
	if !ok {
		pp("can't get fow for player; player:", player)
	}
	return fow
}

func (s *FowSys) updateFowForUnit(u *ecs.Entity, cx, cy int) {
	_log.Inf("[FowSys] updateFowForUnit;", u, cx, cy)
	// Unmask for each player
	for _, pl := range _PlayerSys.players {
		uc := c_Unit(u)
		// If its a player's unit
		if uc.player == pl {
			fow := pl.getFow()
			// Unmask at units position
			// TODO: needs larger unmask area
			singlePoint := false
			if singlePoint {
				fow.mask[cx][cy] = false
			} else {
				viewRange := uc.getViewRangeCells()
				fow.fillCircle(cx, cy, viewRange, false)
			}
		}
	}
}

func (s *FowSys) update(dt float64) {

}
