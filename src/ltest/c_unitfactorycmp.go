package main

import (
	. "fmt"
	"reflect"

	"lib/ecs"
)

// Component
type UnitFactoryCmp struct {
	ProdQueue *ProdQueue
}

func newUnitFactoryCmp(en *ecs.Entity, camp *Camp) *UnitFactoryCmp {
	uf := &UnitFactoryCmp{
		ProdQueue: newProdQueue(camp),
	}
	en.AddComponent(uf)
	return uf
}

func ckcmp(c interface{}) {
	if reflect.ValueOf(c).IsNil() {
		v := reflect.TypeOf(c).Elem()
		Println("type: ", v.Name())
		panic("Check component failed.")
	}
}

func (f *UnitFactoryCmp) produce(utype string, player *Player) {
	// Check component
	ckcmp(f)

	// Spawn unit
	//u := MakeUnit(utype, camp).(UnitI)
	u := makeUnit(utype, player).Get(UnitT).(*Unit)
	u.spawn()
}
