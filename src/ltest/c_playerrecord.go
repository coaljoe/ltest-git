package main

import (
	"lib/ecs"
	"math/rand"
)

// Отвечает за принадлежноть Entity к Player.
// Component
type PlayerRecord struct {
	*ecs.Component
	//camptype *CampType
	player *Player
	//camp   *Camp // Link?
}

//func (cr *PlayerRecord) camp() *Camp                { return cr.camp }
func (pr *PlayerRecord) getPlayerRecord() *PlayerRecord { return pr }
func (pr *PlayerRecord) setPlayer(p *Player)            { pr.player = p }
func (pr *PlayerRecord) getPlayer() *Player             { return pr.player }

func newPlayerRecord(en *ecs.Entity) *PlayerRecord {
	cr := &PlayerRecord{
		Component: ecs.NewComponent(),
		//camp: game.campsys.getDefaultCamp(),
		player: getPlayer(),
	}
	en.AddComponent(cr)
	return cr
}

func (pr *PlayerRecord) hasPlayer() bool {
	return pr.player != nil
}


/*
func (cc *PlayerRecord) setCampId(cid CampId) {
	// Set camp by campid
	c := game.campsys.camps[cid]
	if c != nil {
		cc.setPlayer(c)
	} else {
		panic(fmt.Sprintf("no such camp in the game; cid: %v", cid))
	}
}
*/
/*
// Set camp by CampId, if there's only one camp of such type
func (cc *PlayerRecord) setCampId(cId CampId) {
	cs := game.campsys.getCampsByCampId(cId)
	if len(cs) > 1 {
		pp("ambigious;", len(cs))
	}
	c := cs[0]
	if c != nil {
		cc.setPlayer(c)
	} else {
		panic(fmt.Sprintf("no such camp in the game; cid: %v", cId))
	}
}
*/

// Set random camp.
func (pr *PlayerRecord) setRandomPlayer() {
	ps := _PlayerSys.players
	id := rand.Intn(len(ps))
	pr.setPlayer(ps[id])
}

func (pr *PlayerRecord) delete() {
	println("PlayerRecord.Delete")
}

func (pr *PlayerRecord) update(dt float64) {
}
