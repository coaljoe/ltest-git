package main

import (
	"fmt"
	. "rx/math"
)

type PositionSys struct {
	*GameSystem
}

func newPositionSys() *PositionSys {
	s := &PositionSys{}
	s.GameSystem = newGameSystem("PositionSys", "Position system", s)
	return s
}

func (s *PositionSys) update(dt float64) {
	//entities := _World.
	//p(s.Elems())
	//pp(s)
	for _, xi := range s.getElems() {
		p := xi.(*Position)
		obj := GetEntity(p).Get(ObjT).(*Obj)
		//var _ = p

		// Dir update
		p.dir.update(dt)
		obj.SetRotZ(p.dir.angle()) // Always set orientation

		//newPos := obj.Pos().Add(Vec3{1 * dt, 1 * dt, 0})

		if p.dPos_ == nil {
			continue
		} else {
			// Begin move
			//p.stepTime = 0
		}

		// Check
		if p.stepTime == 0 {
			p.stepTime = game.timer.time
		}

		doneMoving := false

		sPos := obj.pos2()
		dPos := p.dPos2()

		//nextPos := sPos

		if p.mtype == PositionMovingType_Lerp {

			// Total time of travel A -> B [constant]
			tt := (cell_size / p.speed) * 10
			//tt := 10.0
			//tt := (float64(cell_size) / 40.0) * 10.0
			//sPos := Pos{u.x * cell_size, u.y * cell_size}
			//sPos := obj.Pos2()
			//dPos := p.dPos

			//fmt.Println(sPos, dPos, p.speed, tt)

			step := (game.timer.time - p.stepTime) / tt
			nextPos := Lerp2(sPos, dPos, step)
			//fmt.Println("step", fmt.Sprintf("%f", step))
			//fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))

			if roundPrec(step, 2) < 1.0 {
				obj.setPos2(nextPos)
			} else {
				obj.setPos2(p.dPos2())
				doneMoving = true
			}

		} else if p.mtype == PositionMovingType_Linear {
			/*
				//panic("not implemented")
				step := 0.02
				nextPos := Pos{geomLerp(sPos.x, dPos.x, step),
					geomLerp(sPos.y, dPos.y, step)}
				nextPos3 := Vec3{nextPos.x, nextPos.y, 0}
				dPos3 := Vec3{dPos.x, dPos.y, 0}

				if !nextPos3.ApproxEqual(dPos3, 0.1) {
					obj.SetPos2(nextPos)
				} else {
					obj.SetPos2(*p.dPos)
					doneMoving = true
				}
			*/

			if p.speed == 0.0 {
				continue
			}

			/*
				tt := 2.0
				fmt.Println(p.speed, tt)

				rate := 1.0 / tt

				if p.i < 1.0 {
					p.i += dt * rate
					nextPos := Pos{geomLerp(sPos.x, dPos.x, p.i),
						geomLerp(sPos.y, dPos.y, p.i)}
					obj.SetPos2(nextPos)
				} else {
					doneMoving = true
					obj.SetPos2(*dPos)
					p.i = 0
				}
			*/

			/*
				change := dt * (p.speed / 10.0)

				curVal := Vec3{obj.PosX(), obj.PosY(), 0}
				targetVal := Vec3{dPos.X(), dPos.Y(), 0}
				fmt.Println("pre p.speed", p.speed, curVal, targetVal, change)

				if (curVal.X() < targetVal.X()) && (curVal.Y() < targetVal.Y()) {
					curVal = curVal.AddScalar(change)
					fmt.Println("+curval", curVal, change, dt)
					if (curVal.X() > targetVal.X()) && (curVal.Y() > targetVal.Y()) {
						curVal = targetVal
					}
				} else {
					curVal = curVal.SubScalar(change)
					fmt.Println("-curval", curVal, change, dt)
					if (curVal.X() < targetVal.X()) && (curVal.Y() < targetVal.Y()) {
						curVal = targetVal
					}
				}

				fmt.Println("p.speed", p.speed, curVal, targetVal, change)

				if curVal != targetVal {
					obj.SetPos(curVal)
				} else {
					doneMoving = true
					obj.SetPos2(p.DPos2())
				}
			*/

			// From:
			//  http://stackoverflow.com/questions/33092912/moving-a-panel-on-its-x-a-certain-amount-in-unity-5-2
			// See also:
			//  https://www.reddit.com/r/Unity3D/comments/41x6zt/vector3movetowards_without_slow/
			// Total time of travel A -> B [constant]
			//tt := (cell_size / p.speed) * 10
			tt := 100.0
			//tt := (float64(cell_size) / 40.0) * 10.0
			//sPos := Pos{u.x * cell_size, u.y * cell_size}
			//sPos := obj.Pos2()
			//dPos := p.dPos

			//fmt.Println(sPos, dPos, p.speed, tt)

			//distCovered := (game.timer.time - p.stepTime) * dt
			distCovered := (game.timer.time - p.stepTime) * p.speed
			step := distCovered / tt
			nextPos := Lerp2(sPos, dPos, step)
			//fmt.Println("step", fmt.Sprintf("%f", step))
			//fmt.Println(fmt.Sprintf("%f %f", game.timer.time, s.stepTime))

			if roundPrec(step, 2) < 1.0 {
				obj.setPos2(nextPos)
			} else {
				obj.setPos2(p.dPos2())
				doneMoving = true
			}

		} else {
			panic(fmt.Sprintf("unknown moving type: %v", p.mtype))
		}

		if doneMoving {
			p.dPos_ = nil
			//p.stepTime = 0
			println("done moving")
		}

		//obj.SetPos(newPos)
	}
}
