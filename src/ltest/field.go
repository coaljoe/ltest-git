package main

import (
	"fmt"
	"lib/ecs"
	ps "lib/pubsub"
	. "rx/math"
)

type Field struct {
	origin     Vec3
	w, h       int
	cells      [][]Cell
	hm         HeightMap
	hmScale    int
	objects    []*FieldObject // Static field objects
	generated  bool
	view       FieldView
	d_hmScaled HeightMap // For debug
}

// Cannot be called directly: use game.field
func _newField() *Field {
	f := &Field{
		origin:     Vec3Zero,
		hm:         newHeightMap(),
		hmScale:    2, // The HeightMap is times bigger than the field
		objects:    make([]*FieldObject, 0),
		d_hmScaled: newHeightMap(),
	}
	// Generate default empty field 128x128
	//f.generateDefault()
	sub(ev_building_spawn, f.onBuildingSpawn)
	return f
}

func (f *Field) onBuildingSpawn(ev *ps.Event) {
	b := ev.Data.(*ecs.Entity)
	// Automatically place buildings
	f.placeBuilding(b)
	//pp("derp")
}

func (f *Field) spawn() {
	if !f.generated {
		pp("field wasn't generated")
	}
	f.view = newFieldView(f)
	f.view.spawn()

	// Spawn field objects
	for _, fo := range f.objects {
		fo.spawn()
	}
}

// General field generate function.
func (f *Field) generate(w, h int, hmW, hmH int,
	hmOpt HeightMapOptions, fgOpt FieldGenerateOptions) {
	f.w = w
	f.h = h

	// Generate cells
	f._generateCells()

	// Generate heightmap
	f.hm.generate(hmW, hmH, hmOpt)
	//f.hm.load("tmp/hm_test1.png")
	//f.hm.load("tmp/hm_test4.png")

	f.d_hmScaled.generate(f.w, f.h, hmOpt)
	f.d_hmScaled.clear()

	// Generate zlev-s
	p("Generate zlev-s:")
	hmScaleFactor := 4 // (256x256/32x32)/2 fixme: hardcoded.
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			//z := f.hm.data[x*2][y*2] // Fixme: use average from nearest cells
			zVals := make([]int, 0)
			for i := 0; i < f.hmScale*hmScaleFactor; i++ {
				for j := 0; j < f.hmScale*hmScaleFactor; j++ {
					s := f.hmScale * hmScaleFactor
					xx, yy := x*s+i, y*s+j
					//p("xx:", xx, "yy:", yy)
					zVals = append(zVals, f.hm.data[xx][yy])
				}
			}
			zSum := 0
			for _, v := range zVals {
				zSum += v
			}
			zAvg := zSum / len(zVals)

			p(zVals, zSum, zAvg, f.hm.data[x][y])

			z := zAvg

			//p(z)
			cell := &f.cells[x][y]
			cell.z = ZLevel(z)
			//p("cell.z=", cell.z)

			f.d_hmScaled.data[x][y] = z
		}
	}
	p("done")
	//pp(2)

	f.generated = true

	// Generate extra objects and such
	generateFieldObjects(f, fgOpt)
	//generateFieldTerrainFeatures()
	//generateFieldCampSites()
	generateFieldZones(f, fgOpt)

	_FowSys.generateFows(w, h)

	// Debug
	f.hm.save(vars.tmpDir + "/hm.png")
	f.hm.saveTxt(vars.tmpDir + "/hm.txt")

	f.d_hmScaled.save(vars.tmpDir + "/hm_scaled.png")
	f.d_hmScaled.saveTxt(vars.tmpDir + "/hm_scaled.txt")
}

func (f *Field) loadHeightMap(path string) {
	f.hm.load(path)
}

func (f *Field) isCellOpen(x, y int) bool {
	if f.cells[x][y].open {
		return true
	} else {
		return false
	}
}

func (f *Field) isCellPassable(x, y int, r RealmType) bool {
	if r == RealmType_Ground && f.cells[x][y].z == 0 {
		return true
	} else if r == RealmType_Navy && f.cells[x][y].z < 0 {
		return true
	}
	return false
}

func (f *Field) size() int {
	// Always square field
	return f.w
}

// Cell height at x, y.
func (f *Field) cellHeightAt(x, y int) ZLevel {
	return f.cells[x][y].z
}

func (f *Field) getHmValueAt(x, y int) int {
	xx, yy := x, y
	if x >= f.hm.w {
		xx = xx - 1
	}
	if y >= f.hm.h {
		//return 0
		yy = yy - 1
	}
	//return f.hm.data[x][y]
	return f.hm.data[xx][yy] // fixme
}

// Heightmap height at x, y.
func (f *Field) heightMapHeightAt(x, y int) int {
	return f.hm.data[x][y]
}

// Special version for fieldview.
// Clamps map's border if index overflow.
// Fixme: use npot heightmap? (size+1px)
func (f *Field) xHeightMapHeightAt(x, y int) int {
	xx, yy := x, y
	if x >= f.hm.w {
		xx = xx - 1
	}
	if y >= f.hm.h {
		//return 0
		yy = yy - 1
	}
	return f.hm.data[xx][yy]
}

func (f *Field) getCellNeighbours(p CPos) []Cell {
	return f.getCellNeighboursDepth(p, 1)
	/*
		r := make([]Cell, 0)

		cx, cy := p.x, p.y
		if cx < 0 || cy < 0 {
			pp("cx or cy is less than zero;", cx, cy)
		}

		next := func(x, y int) {
			// Filter negative
			if x < 0 || y < 0 {
				return
			}
			c := f.cells[x][y]
			r = append(r, c)
		}

		next(cx-1, cy+1) // Top Left
		next(cx, cy+1)   // Top Center
		next(cx+1, cy+1) // Top Right

		next(cx-1, cy) // Center Left
		next(cx+1, cy) // Center Right

		next(cx-1, cy-1) // Bottom Left
		next(cx, cy-1)   // Bottom Center
		next(cx+1, cy-1) // Bottom Right

		return r
	*/
}

// Get rectangular slice of cells.
func (f *Field) getRectSlice(cx, cy, w, h int) []Cell {
	//ret := make([]Cell, w*h)
	ret := make([]Cell, 0)
	p(len(ret), ret)
	for y := cy; y < h+cy; y++ {
		for x := cx; x < w+cx; x++ {
			//p("->", x, y)
			if x < 0 || y < 0 {
				// Skip
				continue
			}
			ret = append(ret, f.cells[x][y])
			//ret[x*w+y] = f.cells[cx+x][cy+y]
		}
	}
	p(len(ret), ret)
	/*
		// Will work only with *nil version
		if Debug && len(ret) != w*h {
			pp("bad len:", len(ret))
		}
	*/
	return ret
}

func (f *Field) getCellNeighboursDepth(p CPos, depth int) []Cell {
	//ret := make([]Cell, 0)
	/*
		ns := f.getCellNeighbours(p)
		for _, n := range ns {
			_ = n
			//nns := f.get
		}
	*/
	if depth < 1 {
		pp("depth should be > 0;", depth)
	}
	if p.x < 0 || p.y < 0 {
		pp("cell pos should be zero or positive;", p)
	}
	size := 3 + ((depth - 1) * 2)
	println("size:", size)
	ret := f.getRectSlice(p.x-1, p.y-1, size, size)
	// Remove central cell
	//i := size / 2 // XXX BAD
	i := len(ret) / 2
	ret = append(ret[:i], ret[i+1:]...)

	return ret
}

func (f *Field) getCells() []Cell {
	ret := make([]Cell, f.w*f.h)
	for y := 0; y < f.h; y++ {
		for x := 0; x < f.w; x++ {
			ret[x*f.w+y] = f.cells[x][y]
			//pp("herp")
			//if f.cells[x][y].z > 0 {
			//	pp("derp")
			//}
		}
	}
	return ret
}

func (f *Field) getCellsAtZLevel(zlev ZLevel) []Cell {
	return f.getCellsAtZLevels(zlev, zlev)
}

func (f *Field) getCellsAtZLevels(zmin, zmax ZLevel) []Cell {
	if zmin > zmax {
		pp("zmin > zmax;", zmin, zmax)
	}
	ret := make([]Cell, 0)
	for _, c := range f.getCells() {
		p("c.z=", c.z)
		if c.z >= zmin && c.z <= zmax {
			ret = append(ret, c)
		}
	}
	return ret
}

// Поставить здание на карту.
func (f *Field) placeBuilding(en *ecs.Entity) {
	b := c_Building(en)
	w, h := int(b.cellSpace.X()), int(b.cellSpace.Y())
	px, py := b.cellPos.x, b.cellPos.y
	for y := py; y < py+h; y++ {
		for x := px; x < px+w; x++ {
			f.cells[x][y].open = false
			p("mark x, y closed", x, y, w, h, px, py)
		}
	}
	//pp("derp")
}

func (f *Field) _generateCells() {
	w, h := f.w, f.h

	cells := make([][]Cell, w) // Rows
	for i := range cells {
		cells[i] = make([]Cell, h) // Cols
	}

	for y := 0; y < h; y++ {
		for x := 0; x < w; x++ {
			cells[x][y] = Cell{
				open: true,
				z:    0,
				pos:  CPos{x, y}, // Redundant
			}
		}
	}

	f.cells = cells
}

func (f *Field) update(dt float64) {
	f.view.update(dt)

	// Update field objects
	for _, fo := range f.objects {
		fo.update(dt)
	}
}

/////////
// Cell

type Cell struct {
	// Redundant but useful. use with caution.
	pos  CPos
	z    ZLevel
	open bool
	// Cell holder, default nil (no-one)
	//playerId int
	holder *Player
}

// Is the cell hold by a player.
func (c Cell) isHoldByPlayer(pl *Player) bool {
	return c.holder == pl
}

func (c Cell) String() string {
	return fmt.Sprintf("Cell<%d,%d>", c.pos.x, c.pos.y)
}

// Cell pos
type CPos struct {
	x, y int
}
