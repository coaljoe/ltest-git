package main

import "os"

const (
	world_size = 1024 / 4
	//cell_size = world_size / 128 // Warning, must be int?
	// Visual cell dimentions in units
	cell_size      = 8 //6
	half_cell_size = cell_size / 2
	//water_zlevel   = -1.0 // Fixme: move to Water?
	zelev_sea0    = 0.0
	zelev_ground0 = 1.0 // Land0
	Debug         = true
	AppName       = "ltest"
)

var (
	fieldH, fieldW int
	BuildRevision  string = "unknown"
)

var vars struct {
	locateTargetsInterval float64
	scrollSpeed           float64
	// Scroll speed when shift is pressed
	scrollSpeedFast   float64
	scrollPadding     int
	debugFieldGrid    bool
	debugFieldZLevels bool
	debugPathfinding  bool
	// Screen resolution
	resX int
	resY int
	// Native screen resolution for fixed-size elements
	nativeResX int
	nativeResY int
	// Resolution scale coeff. for fixed-size elements
	resScaleX float64
	resScaleY float64
	// Wrapped world space is used
	wrapped bool
	// For temporary files
	tmpDir    string
	gameSpeed float64
	// Time Delta
	dt float64
	// Game speed-independant Time Delta
	dtGameSpeed float64
}

func SetDefaultVars() {
	vars.locateTargetsInterval = 4.0
	vars.scrollSpeed = 25.0
	//vars.scrollSpeedFast = 100.0
	vars.scrollSpeedFast = vars.scrollSpeed * 6
	vars.scrollPadding = 40
	vars.debugFieldGrid = false
	vars.debugFieldZLevels = false
	vars.debugPathfinding = false
	vars.resX = 1024
	vars.resY = 576
	vars.nativeResX = 1024
	vars.nativeResY = 576
	vars.resScaleX = 1
	vars.resScaleY = 1
	vars.wrapped = false
	vars.gameSpeed = 1
	vars.dt = 0
	vars.dtGameSpeed = 0

	vars.tmpDir = os.TempDir()
	//pp(vars.tmpDir)
}
