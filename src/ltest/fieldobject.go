package main

type FieldObject struct {
	//obj       *Obj
	//*Obj
	*SimpleObj
	typ       string // FieldObject's type
	removable bool
	passable  bool
	view      *FieldObjectView
}

func newFieldObject(typ string) *FieldObject {
	fo := &FieldObject{
		SimpleObj: newSimpleObj(),
		typ:       typ}
	fo.load()

	// Set Z Position
	//fo.SetZ(0.1)
	return fo
}

func (fo *FieldObject) load() {
	if fo.typ == "" {
		panic("fieldobject's type not set")
	}

	// Object factory
	switch fo.typ {
	case "stone":
		{
			fo.typ = "stone"
		}
	case "tree":
		{
			fo.removable = true
		}
	default:
		p("unknown fieldobject type:", fo.typ)
	}
}

func (fo *FieldObject) spawn() {
	fo.view = newFieldObjectView(fo)
}

func (fo *FieldObject) update(dt float64) {
	// Update view
	if fo.view != nil {
		fo.view.update(dt)
	}
}
