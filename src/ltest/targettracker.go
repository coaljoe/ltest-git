// Модуль для отслеживания (трекинга) целей, хост и клиент.
package main

import (
	"lib/ecs"
)

/***** TtTargetable *****/

// Цель которую атакуют. (Хост)
type TtTargetable struct {
	targetedBy map[*ecs.Entity]*TtTargeter
}

func newTtTargetable() *TtTargetable {
	return &TtTargetable{
		targetedBy: make(map[*ecs.Entity]*TtTargeter),
	}
}

func (tth *TtTargetable) addTargetedBy(att *ecs.Entity, cl *TtTargeter) {
	tth.targetedBy[att] = cl
}

func (tth *TtTargetable) removeTargetedBy(att *ecs.Entity) {
	delete(tth.targetedBy, att)
}

func (tth *TtTargetable) cleanTargetedBy() {
	_log.Dbg("-- cleanTargetBy:")
	_log.Dbg("len targetedBy ", len(tth.targetedBy))
	for att, cl := range tth.targetedBy {
		//println("derp")
		//cl.target = nil
		p(att, cl.target)

		c_Combat(cl.target).unsetTargets()
		c_Combat(att).unsetTargets()

		cl.unsetTarget()
		//tth.removeTargetedBy(att)
		//cl.SetTarget(nil)
		var _ = att
	}
	_log.Dbg("len targetedBy ", len(tth.targetedBy))
	_log.Dbg("-- end")
}

/***** TtTargeter *****/

// Атакующий цели. (Клиент)
type TtTargeter struct {
	target *ecs.Entity
}

func newTtTargeter() *TtTargeter {
	return &TtTargeter{}
}

func (ttc *TtTargeter) setTarget(t *ecs.Entity) {
	ttc.target = t
	if t != nil {
		c_Combat(t).getTtHost().addTargetedBy(t, ttc)
	} else {
		//c_Combat(t).getTtHost().removeTargetedBy(t)
		pp("target cannot be nil")
	}
}

func (ttc *TtTargeter) unsetTarget() {
	c_Combat(ttc.target).getTtHost().removeTargetedBy(ttc.target) // XXX
	ttc.target = nil
}

//func (ttc *TtTargeter) unsetTarget() {
//	ttc.target = nil
//}

/*
func (ttc *TTClient) UnsetTarget() {
  ttc.target = nil
  ttc.host.targetedBy -= t
}
*/
