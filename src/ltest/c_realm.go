package main

import "lib/ecs"

// Interface component
type RealmI interface {
	getRealm() RealmType
}

func getRealm(en *ecs.Entity) RealmType {
	var realm RealmType
	// Iterate over components (fixme: move to ecs(?))
	found := false
	for _, ci := range en.Components() {
		if c, ok := ci.(RealmI); ok {
			realm = c.getRealm()
			found = true
			break
		}
	}
	if !found {
		panic("realm component not found")
	}
	return realm
	//panic("not reached")
}
