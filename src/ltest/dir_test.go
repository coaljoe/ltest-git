// +build !static

package main

// Run with LOGLEVEL=TRACE

import (
	. "fmt"
	"testing"
	//"os"
	//. "ltest"
)

func TestDir(t *testing.T) {
	/*
	  gamedir := "/home/j/dev/misc/ltest/go"
	  os.Chdir(gamedir)

	  Init()
	*/

	initTest()

	d := newDir()

	if !d.locked() {
		//t.Fail()
		//t.FailNow()
		t.Fatal("Locked() fail")
	}

	//d.rotTo(10, 1)
	//d.rotTo(270, 1)
	d.rotTo(-180, 1)
	//d.rotTo(-180, 0.01)
	//d.Update(1)
	Println(d.angle())
	for i := 0; i < 10; i++ {
		d.update(1)
		Println(Sprintf("%d:", i), d.angle())
	}

	Println(d.sRot(), d.dRot())

	if !d.locked() {
		//t.Fail()
		//t.FailNow()
		t.Fatal("Locked() fail")
	}
}
