open Printf
open Gg
(*open Aux3d*)
open Camera
open Node
open Transform
(*
open Camera
let _ = 
    print_endline "test";;
    let x = 123;;
    print_endline "test2";;
    let c = new camera ;;
    (*c.pos <- V3.v 0 0 -10 ;; *)
    (*c#pos <- V3.zero ;;*)
    (*c#set_pos @@ V3.v 0. 0. -10. ;;*)
    let v = V3.v 0.1 0.1 0.1;;
    c#set_pos V3.zero ;;
    c#set_pos v;;
    c#set_pos @@ V3.v 0.0 0.1 (-.10.1) ;;
    c#print ;;
    (*V3.pp c#pos;;*)
    print_endline (V3.to_string c#pos) ;;
*)

let _ = 
    print_endline "test";;
    let x = 123;;
    print_endline "test2";;
    let c = Camera.create;;
    (*c.pos <- V3.v 0 0 -10 ;; *)
    (*c#pos <- V3.zero ;;*)
    (*c#set_pos @@ V3.v 0. 0. -10. ;;*)
    let v = V3.v 0.1 0.1 0.1;;
    Node.set_name "camera1" c;;
    (*Camera.set_pos V3.zero c;;*)
    (*Transform.set_pos V3.zero c;;*)
    (*Node.set_pos c V3.zero;;*)
    (*c.set_pos V3.zero ;;*)
    (*c.set_pos v;;*)
    (*c.set_pos @@ V3.v 0.0 0.1 (-.10.1) ;;*)
    Camera.print c ;;
    (*V3.pp c#pos;;*)
    (*print_endline (V3.to_string c#pos) ;;*)
