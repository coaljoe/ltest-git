open Printf

type node = {mutable name: string}
type camera = {mutable x: int}

module N = struct
  let setn s x = s.name <- x
  let print_node s = printf "N print (name=%s) \n" s.name
end

module C = struct
  include N
  type t = camera * node

  let create = {x = 1}
  let setx x s = s.x <- x
  let print s = printf "X print (x=%d) \n" s.x
end

  
let _ = 
  let c = C.create in
  C.print_node c;;
  C.print c;;
  ()
;;
