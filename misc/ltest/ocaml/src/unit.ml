open Vector3

class unit =
  object
    val mutable pos = Vector3 make
    method pos = pos
    method test = Printf.printf "unit.test\n"
  end

let () = 
  Printf.printf "test\n";;
  Printf.printf "test2\n";;
  let u1 = new unit;;
  u1#test;;
  Printf.printf u1#pos.(0);;

