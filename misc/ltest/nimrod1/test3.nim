type
  A = ref object # XXX needs ref
    name*: string
    id*: int

proc setA(a: A) =
  a.id = 123

proc newA(): A =
  new(result)
  result.id = 321
  result.setA()
  #initA(result)

var z = newA()
echo z.id
echo "done"