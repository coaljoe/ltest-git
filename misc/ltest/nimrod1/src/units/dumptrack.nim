import obj, unit

type
  DumpTruck* = ref object of Unit

proc newDumpTruck*(): DumpTruck =
  new result
  initObj(result)
