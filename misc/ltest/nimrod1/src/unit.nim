#import obj
from obj import Obj
from building import Building

type
  Unit* = ref object of Obj
    typename: string
    mass: float

proc takeDamage*(u: Unit) =
  echo "::takeDamage"



# trait
type Attackable* = generic x
  #reload(x)
  takeDamage(x)

proc attack*(u: Unit, oth: Unit) =
  echo "attack"

proc attack*(u: Unit, b: Building) =
  echo "attack building"

proc attack_g1*(a: Attackable, b: Attackable) =
  echo "attack_g1 "

proc attack_g2*(a: Attackable) =
  echo "attack_g2 "
