import unit, units/tank, units/dumptrack, building

proc main() =
  let u1 = newTank()
  let u2 = newTank()
  echo u1.id, " ", u1.name
  echo u2.id, " ", u2.name
  u1.attack(u2)
  attack(u1, u2)

  # test building
  let b1 = newBuilding()
  u1.attack(b1)

  # test dumptruck
  let dt1 = newDumpTruck()
  u1.attack(dt1)
  dt1.attack(u1)

  #discard
  # test trait
  attack_g2(u1)
  attack_g2(u2)
  attack_g2(dt1)
  attack_g1(u1, u2)
  attack_g1(u2, u1)
  attack_g1(dt1, dt1)
  u1.attack_g1(u2)
  #attack_g1(dt1, u1) # error
  #u1.attack_g1(dt1)
  #dt1.attack_g1(u1)
  #attack_g2(b1)

main()
