## Tasks
type
  TaskPriority = enum
    default, low, normal, high

  DeptTask* = ref object of TObject
    name*: string
    description*: string
    priority*: TaskPriority

type
  Dept* = ref object of TObject
    name*: string
    description*: string
    tasks*: seq[DeptTask]

proc initDept*(d: Dept, name: string) =
  d.name = name
  d.tasks = newSeq[DeptTask]()

method addTask*(d: Dept, t: DeptTask) {.procvar.} =
  d.tasks.add(t)

method addTask1*[T](a: T, t: DeptTask) =
  echo "task1"
  a.tasks.add(t)

method listTasks*(d: Dept) =
  echo "listing tasks..."
  for t in d.tasks:
    echo "Task name=", t.name
  echo "done"

## Tasks

proc newDeptTask*(name: string): DeptTask =
  new result
  result.name=name
  result.priority=TaskPriority.default
