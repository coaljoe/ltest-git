import dept, depts/constdept

proc test() =
  var t = newDeptTask(name = "Test Task")
  var cd = newConstDept()
  cd.addTask(t)
  cd.listTasks()

if isMainModule:
  test()
