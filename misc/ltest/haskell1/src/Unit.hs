module Unit where
import Data.Vect.Float.Base

--type Pos2 = Float Float
type Pos2 = Vec2

-- Тип юнитов

data UnitType = Tank
              | DumpTruck
              | LightTank
  deriving (Show, Eq)

data Unit = Unit { ident :: Int,
                   utype :: UnitType,
                   pos :: Pos2,
                   health :: Int,
                   speed :: Float }
  deriving Show
instance Eq Unit where
    a == b = ident a == ident b

--instance Show Unit where


-- Функции

makeUnit utype =
    let u = Unit { ident = 0, utype = utype, pos = Vec2 0 0, health = 100, speed = 0.0 } in
    u

attack u =
    case utype u of
     DumpTruck -> print "DumpTruck can't attack"
     LightTank -> print "Tank can attack"
     _ -> print "error, unknow utype"