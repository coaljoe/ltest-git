extern crate debug;
use std::vec::Vec;

struct A {
    x: int,
}

struct B {
    x: int,
    y: int
}

fn print_type_of<T>(_: &T) -> () {
    let type_name =
        unsafe {
            (*std::intrinsics::get_tydesc::<T>()).name
        };
    println!("{}", type_name);
}

impl A {
    fn update(&self) {
        println!("A update");
    }
}

impl B {
    fn update(&self) {
        println!("B update");
    }
}

trait Unit {
    /*
    fn update(&self) {
        println!("lol");
        //self.update();
    }
    */
    fn update(&self);
}
impl Unit for A {
    fn update(&self) {
        println!("A.trait update");
        print_type_of(self);
        println!("{:?}", *self)
    }
}
impl Unit for B {
    fn update(&self) {
        println!("B.trait update");
        print_type_of(self);
        println!("{:?}", *self)
    }
}


fn main() {
    let u1 = box A { x: 11 };
    let u2 = box B { x: 12, y: 0 };
    u1.update();
    u2.update();
    
    let mut v: Vec<Box<Unit>> = Vec::new();
    v.push(u1);
    v.push(u2);
    
    for u in v.iter() {
        u.update();
    }
}