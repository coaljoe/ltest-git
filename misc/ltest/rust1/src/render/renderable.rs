pub trait Renderable {
  fn render(&self);
}

pub trait NonRenderable {
  fn render(&self) {}
}