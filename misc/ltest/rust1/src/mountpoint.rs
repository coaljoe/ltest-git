use cgmath::Vector2;

use obj::{Obj};

type Pos = Vector2<f32>;

#[deriving(Show)]
pub struct MountPoint {
  pub host: Box<Obj>,
  pub client: Option<Box<Obj>>,
  pub locked: bool,
  offset: Pos,
}

impl MountPoint {
  pub fn new(host: &Obj, offset: Pos) -> MountPoint {
    MountPoint { 
      host: box *host,
      client: None,
      locked: false,
      offset: offset,
    }
  }
  pub fn setClient(&mut self, obj: &Obj) {
    self.client = Some(box *obj);
    self.locked = true;
  }
  pub fn unsetClient(&mut self) {
    self.client = None;
    self.locked = false;
  }
  pub fn update(&self) {
    if !self.locked {
      return
    }
    //self.client.unwrap().pos = self.host.pos + self.offset;
    println!("mountpoint.update");
  }
}