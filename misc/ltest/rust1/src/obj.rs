#![feature(struct_inherit, struct_variant, globs)]

//extern crate cgmath;

use cgmath::Vector2;
//use cgmath::*;
type Pos = Vector2<f32>;

#[deriving(Show)]
pub struct Obj {
  pub id: uint,
  pub name: &'static str,
  pub pos: Pos,
}

static mut maxObjId: uint = 0;
impl Obj {
  pub fn new(name: &'static str) -> Obj {
    let newId;
    //let newName = name;// || "unnamed";
    unsafe {
      newId = maxObjId;
      maxObjId += 1;
    }
    Obj { id: newId, name: name, pos: Vector2::new(0f32, 0f32)}
  }
}
