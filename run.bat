@echo off
setlocal
call env.bat
for /f "delims=" %%i in ('hg id -i') do set _rev=%%i

@rem always clean
go install -v rx rx/math lib/ecs lib/xlog lib/sr lib/pubsub 
del bin\ltest.exe 2> nul

@rem go run -v src\main.go
go install -v -ldflags="-X 'main.BuildRevision=%_rev%'" ltest 

if "%1" == "-trace" (
  goto trace
) else if "%1" == "-test1" (
  goto test1
) else (
  goto run
)

:trace
shift
apitrace trace -o trace.trace bin\ltest.exe %1 %2 %3 %4 %5
goto end

:test1
@rem test a single test/pattern
@rem usage: run -test1 TestPathfind
go test -v ltest -test.run %2 %3 %4 %5
goto end

:run
bin\ltest.exe %*
goto end

:end
endlocal
