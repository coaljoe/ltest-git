varying vec2 texpos;
uniform sampler2D tex;
uniform vec4 color = vec4(0, 0, 1, 1); // debug blue by default

void main(void) {
  gl_FragColor = vec4(1, 1, 1, texture2D(tex, texpos).a) * color;
}

