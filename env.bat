rem @echo off
rem setlocal
rem set PATH=%PATH%;bin\lib\dll
set PATH=%PATH%;%CD%\bin\lib\freetype
rem set LIBRARY_PATH=%CD%\bin\lib\freetype
set GOPATH=%CD%\..\go;%CD%
rem set GOMAXPROCS=0
set GODEBUG=cgocheck=2
set DEBUG=1

echo %LIBRARY_PATH%

if "%LOGLEVEL%" == "" (
  set LOGLEVEL=INFO
)

@rem set mingw env
rem set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
if exist C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin (
  set PATH=C:\Program Files\mingw-w64\x86_64-5.3.0-posix-seh-rt_v4-rev0\mingw64\bin;%PATH%
) else if exist C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin (
  set PATH=C:\users\k\k\progs\mingw-w64\x86_64-6.2.0-posix-seh-rt_v5-rev1\mingw64\bin;%PATH%
) else if exist C:\Users\k\k\progs\winbuilds\bin (
  set PATH=C:\Users\k\k\progs\winbuilds\bin;%PATH%
)

@rem set apitrace path
set PATH=C:\Users\k\k\progs\apitrace\bin;%PATH%

rem endlocal