#!/bin/bash
## Set env.

export GOPATH=$HOME/dev/go:$PWD
#export TMPDIR=~/build/tmp
#export GOMAXPROCS=0
#export GOMAXPROCS=1
#export GOTRACEBACK=crash
#export GOTRACEBACK=2
export GODEBUG=efence=1
## disable gc
#export GOGC=off
# debug by default
export DEBUG=1

export MESA_DEBUG=1,incomplete_tex,incomplete_fbo
export LIBGL_DEBUG=verbose
#export LIBGL_ALWAYS_SOFTWARE=1 
export MESA_GLSL_CACHE_DISABLE=1

