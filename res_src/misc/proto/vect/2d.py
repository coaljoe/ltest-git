#!/usr/bin/env python

import os, sys
import pygame
from pygame.locals import *
from random import randint, choice

sx = 400
sy = 300
clbg = (0,0,0)
clred = (255,0,0)
delay = 40

pygame.init()
scr = pygame.display.set_mode((sx, sy))

class pos:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def __add__(self, o):
        return pos(self.x + o.x,  self.y + o.y)
    def as_list(self):
        return (self.x, self.y)


class Tank:
    def __init__(self, x, y):
        self.pos = pos(x, y)
        self.rot = 0
    def draw(self):
        #pygame.draw.line(scr, clred, self.pos.x, self.pos.x + 10)
        rp = self.pos + pos(10, 0)
        pygame.draw.line(scr, clred, self.pos.as_list(), rp.as_list())


t1 = Tank(30, 30)

def blit():
    t1.draw()

done = 0
while not done:
    for event in pygame.event.get():
        if event.type == QUIT:
            done = 1
            break

        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()
            if event.key == K_f:
                pygame.display.toggle_fullscreen()
            if event.key == K_w:
                line_width += 1
            elif event.key == K_e:
                line_width -= 1

    scr.fill(clbg)

    blit()

    pygame.display.flip()
    pygame.time.delay(delay)
    #done = 1

