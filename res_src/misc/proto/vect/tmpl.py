#!/usr/bin/env python

import os, sys
import pygame
from pygame.locals import *
from random import randint, choice

sx = 400
sy = 300
clbg = (0,0,0)
delay = 40

pygame.init()
scr = pygame.display.set_mode((sx, sy))

def blit():
    pass

done = 0
while not done:
    for event in pygame.event.get():
        if event.type == QUIT:
            done = 1
            break

        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()
            if event.key == K_f:
                pygame.display.toggle_fullscreen()
            if event.key == K_w:
                line_width += 1
            elif event.key == K_e:
                line_width -= 1

    scr.fill(clbg)

    blit()

    pygame.display.flip()
    pygame.time.delay(delay)
    #done = 1

