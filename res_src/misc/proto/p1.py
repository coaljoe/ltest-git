#!/usr/bin/env python

import os, os.path, sys
import pygame
from pygame.locals import *
from random import randint, choice
from sys import exit

# nastrojki
scr_w = 640
scr_h = 480
line_width = 20
delay = 5 
clblack = (0,0,0)
clwhite = (255,255,255)
tile_h = 38
tile_w = 41
map_w = 5
map_h = 5
f_xo = 93
f_yo = 32
f_xo = 300
f_yo = 100
f_shift = 1

pygame.init()
pygame.key.set_repeat(3, 3)
scr = pygame.display.set_mode((scr_w, scr_h))
im = pygame.image.load("z2.png")
#im1 = pygame.image.load("3434.png")
im1 = pygame.image.load("p1.jpg")
im1 = im1.convert()
#im1.set_colorkey((0xff, 0x00, 0xff))
im1.set_alpha(140)
im1a = pygame.image.load("iso_test5a.png")
im1a = im1a.convert()
im1a.set_colorkey((0xff, 0x00, 0xff))
im2 = pygame.image.load("iso_test_sel.png")
im2 = im2.convert()
im2.set_colorkey((0xff, 0x00, 0xff))\

imgs = []
imgz = []
for img in os.listdir('pix'):
    im = pygame.image.load(os.path.join('pix', img))
    im = im.convert()
    im = pygame.transform.scale(im, (randint(50, 300), randint(100, 150)))
    #im.set_alpha(140)
    imgz.append(im)
    
def refill():
    global imgs
    imgs = []
    for x in xrange(map_h*map_w):
        imgs.append(choice(imgz))

def randrefill():
    global imgs
    for x in xrange(randint(2, 5)):
        imgs[randint(1, len(imgs)-1)] = choice(imgz) 

refill()

#exit()

z = 1.
d = 1
fc = 0

def blit():
    global scr, map_w, map_h, tile_w, tile_h, z, f_yo, f_xo, fc, d
    xsh = ysh = xf = yf = 0
    #xsh_add = -10
    #ysh_add = -8 
    xsh_add = -2
    ysh_add = -2
    yf_add = 7 # 11
    yf_add = z
    xf_add = -6
    xf_add = 0-(yf_add) # 11
    #z *= 1.013
    if z > 150:
        z = 10
    else:
        z += 0.2
    f_yo -= 1
    f_xo += 1
    fc += 1
    #print z
    if fc%5 == 0: randrefill()
    if fc%400 == 0: refill()
    print z


    #scr.blit(im, (0,0,0,0)) 
    for y in xrange(map_h):
        xf += xf_add
        ysh += ysh_add
        xsh = xsh_add
        yf = 0
        for x in xrange(map_w):
            yf += yf_add
            xsh += xsh_add
            #scr.blit(im1, (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
            #scr.blit(choice([im1, im1a]), (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
            #scr.blit(im1, (f_xo + x*tile_w + xsh + xf, f_yo + y*tile_h + ysh + yf)) 
            scr.blit(imgs[y*x], (f_xo + x*tile_w + xsh + xf, f_yo + y*tile_h + ysh + yf)) 
            #print "++blit x:", x, "y:", y, "xsh:", xsh, "xf: ", xf, "ysh:", ysh, "yf", yf
            if x == 1 and y == 1:
                pass
                #scr.blit(im2, (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
    

done = 0
while not done:
    for event in pygame.event.get():
        if event.type == QUIT:
            done = 1
            break

        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()
            if event.key == K_1:
                f_xo += f_shift
                print "f_xo:", f_xo
            if event.key == K_2:
                f_yo += f_shift
                print "f_yo:", f_yo
            if event.key == K_3:
                if f_shift == 1: f_shift = -1
                else: f_shift = 1
            if event.key == K_4:
                if not im1.get_alpha(): im1.set_alpha(127)
                else: im1.set_alpha(0)

    scr.fill(clblack)

    blit()

    pygame.display.flip()
    pygame.time.delay(delay)

