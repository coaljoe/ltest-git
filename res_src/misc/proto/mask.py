#!/usr/bin/env python

import os, sys
import pygame
from pygame.locals import *
from random import randint, choice

#__conf__
scr_w = 320
scr_h = 320
delay = 40
clblack = (0,0,0)
clwhite = (255,255,255)
tile_h = 38
tile_w = 41
map_w = 8
map_h = 8

pygame.init()
scr = pygame.display.set_mode((scr_w, scr_h))
pygame.key.set_repeat(3, 3)
bg = pygame.image.load("grad.png")
bg = bg.convert()
#im1a = pygame.image.load("iso_test5a.png")
#im1a = im1a.convert()
#im1a.set_colorkey((0xff, 0x00, 0xff))

def blit():
    scr.blit(bg, (0,0)) 
    scr.blit(bg, (12, 12, 12, 12))

done = 0
while not done:
    for event in pygame.event.get():
        if event.type == QUIT:
            done = 1
            break

        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()

            if event.key == K_UP:
                delay -= 1
                print "delay: %s ms" % delay
                continue
            elif event.key == K_DOWN:
                delay += 1
                print "delay: %s ms" % delay
                continue

    if delay <= 0: delay = 0

    blit()

    pygame.display.flip()
    pygame.time.delay(delay)

