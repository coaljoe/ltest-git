import Blender
from Blender import *
from Blender.Scene import Render
from Blender import Camera
import sys
import os
from math import degrees, radians

#print "hello world"
args = sys.argv[4:]

scn = Scene.GetCurrent()
context = scn.getRenderingContext()
context.enableExtensions(1)
context.enableRayTracing(0)
context.enableShadow(1)
context.setOversamplingLevel(5)
context.setImageType(Render.PNG)
context.setRenderPath("/tmp/")
context.startFrame(1)
context.endFrame(1)

cam = Blender.Object.Get("maincam")
cam_cam = Camera.Get("maincam")
print dir(cam_cam)
print cam_cam.getScale()
cam.RotX = radians(float(args[2]))
cam.RotY = radians(float(args[3]))
cam.RotZ = radians(float(args[4]))
print degrees(cam.RotX)
print degrees(cam.RotY)
print degrees(cam.RotZ)
#print dir(cam)
#new_loc = list(cam.getLocation())
#new_loc[1] = float(args[1])
#print "new_loc:", new_loc
#cam.setLocation(new_loc)
cam_cam.setScale(float(args[1]))
print cam.getLocation()

#Render.Enable()
#context.render()
context.renderAnim()
Blender.Quit()
