#!/usr/bin/env python

import os, os.path, sys
import pygame
from pygame.locals import *
from random import randint, choice
from sys import exit

#__conf__
scr_w = 640
scr_h = 480
line_width = 20
delay = 30
clblack = (0,0,0)
clwhite = (255,255,255)
tile_h = 38
tile_w = 41
map_w = 32
map_h = 32
f_xo = 6
f_yo = -152
#f_xo = 191
#f_yo = 167
f_shift = 1

cam_roty = 0
#cam_scale = 11.05
#cam_rotx = 12
#cam_rotz = 11.8
cam_scale = 10.81
cam_rotx = 13.9
cam_rotz = 11.9
cmd = ""
dir = "/home/k0w/hg/g2/proto"

def buildbg():
    global cmd, dir
    os.chdir(dir)
    #cmd = "blender -b isotest.blend -P isotest_s1a.py %s %s %s %s" % \
    cmd = "blender -b isotest-z1_na.blend -P isotest_s1a.py %s %s %s %s" % \
        (cam_scale, cam_rotx, cam_roty, cam_rotz)
    print "cmd:", cmd
    os.popen(cmd)

buildbg()

pygame.init()
pygame.key.set_repeat(3, 3)
scr = pygame.display.set_mode((scr_w, scr_h))
bg = None
def loadbg():
    global bg
    im = pygame.image.load("/tmp/0001.png")
    bg = im.convert()
    print "loaded bg"
loadbg()

im1 = pygame.image.load("3534m.png")
im1 = im1.convert()
im1.set_colorkey((0xff, 0x00, 0xff))
im1.set_alpha(140)
#exit()

z = 1.
d = 1
fc = 0

def blit():
    global scr, map_w, map_h, tile_w, tile_h, z, f_yo, f_xo, fc, d
    xsh = ysh = xf = yf = 0
    xsh_add = -12
    ysh_add = -10 
    yf_add = 6 #11
    xf_add = -6

    scr.blit(bg, (0,0,0,0)) 
    for y in xrange(map_h):
        xf += xf_add
        ysh += ysh_add
        xsh = xsh_add
        yf = 0
        for x in xrange(map_w):
            yf += yf_add
            xsh += xsh_add
            #scr.blit(im1, (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
            #scr.blit(choice([im1, im1a]), (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
            #scr.blit(im1, (f_xo + x*tile_w + xsh + xf, f_yo + y*tile_h + ysh + yf)) 
            scr.blit(im1, (f_xo + x*tile_w + xsh + xf, f_yo + y*tile_h + ysh + yf)) 
            #print "++blit x:", x, "y:", y, "xsh:", xsh, "xf: ", xf, "ysh:", ysh, "yf", yf
            if x == 1 and y == 1:
                pass
                #scr.blit(im2, (80 + x*tile_w + xsh + xf, 80 + y*tile_h + ysh + yf)) 
    

done = 0
while not done:
    for event in pygame.event.get():
        if event.type == QUIT:
            done = 1
            break

        if event.type == KEYDOWN:
            if event.key == K_q:
                sys.exit()
            if event.key == K_r:
                buildbg()
                loadbg()
                continue
            if event.key == K_a:
                cam_rotz += 0.1
                print "cam_rotz:", cam_rotz
                continue
            if event.key == K_z:
                cam_rotz -= 0.1
                print "cam_rotz:", cam_rotz
                continue
            if event.key == K_s:
                cam_rotx += 0.1
                print "cam_rotx:", cam_rotx
                continue
            if event.key == K_x:
                cam_rotx -= 0.1
                print "cam_rotx:", cam_rotx
                continue
            if event.key == K_d:
                cam_scale += 0.02
                print "cam_scale:", cam_scale
                continue
            if event.key == K_c:
                cam_scale -= 0.02
                print "cam_scale:", cam_scale
                continue
            if event.key == K_1:
                f_xo += f_shift
                print "f_xo:", f_xo
            if event.key == K_2:
                f_yo += f_shift
                print "f_yo:", f_yo
            if event.key == K_3:
                if f_shift == 1: f_shift = -1
                else: f_shift = 1
                continue
            if event.key == K_4:
                if not im1.get_alpha(): im1.set_alpha(127)
                else: im1.set_alpha(0)

    scr.fill(clblack)

    blit()

    pygame.display.flip()
    pygame.time.delay(delay)

