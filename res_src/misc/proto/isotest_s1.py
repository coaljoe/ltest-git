import Blender
from Blender import *
from Blender.Scene import Render
#from Blender import Camera
import os

print "hello world"
print os.environ

scn = Scene.GetCurrent()
context = scn.getRenderingContext()
context.enableExtensions(1)
context.enableRayTracing(0)
context.enableShadow(0)
context.setOversamplingLevel(5)
context.setImageType(Render.PNG)
context.setRenderPath("/tmp/")
context.startFrame(1)
context.endFrame(1)

cam = Blender.Object.Get("maincam")
print cam.getLocation()
print cam

Render.EnableDispWin()
#context.render()
context.renderAnim()