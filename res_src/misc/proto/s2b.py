#!/usr/bin/env python

import Blender
from Blender import *
from Blender.Scene import Render
#from Blender import Camera

print "hello world"

Blender.Load("isotest.blend")

scn = Scene.GetCurrent()
context = scn.getRenderingContext()
context.enableExtensions(1)
context.enableRayTracing(0)
context.enableShadow(0)
context.setOversamplingLevel(5)
context.setImageType(Render.PNG)
context.setRenderPath("/tmp/")
context.startFrame(1)
context.endFrame(1)

cam = Blender.Object.Get("maincam")
print cam.getLocation()
print cam

#Render.Enable()
#context.render()
context.renderAnim()
Blender.Quit()
